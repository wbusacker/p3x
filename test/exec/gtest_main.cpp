#include <gtest/gtest.h>

extern "C" {
#include <log.h>
}

int main(int argc, char** argv) {
    init_log(argv[0]);
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}