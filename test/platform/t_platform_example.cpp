/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: platform_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <platform_functions.h>
}
