/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <p3x_error_functions.h>
}
