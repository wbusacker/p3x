/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  server
Filename: test_engine_template.c
Purpose:  Example Function File
*/

#include <stdio.h>
#include <string.h>
#include <test_engine_functions.h>

bool test_engine_comparison(enum Test_engine_comparison_mode mode,
                            enum Test_engine_comparison_type type,
                            char*                            file_name,
                            uint64_t                         file_line,
                            int64_t                          ix,
                            int64_t                          iy,
                            uint64_t                         ux,
                            uint64_t                         uy,
                            double                           fx,
                            double                           fy) {
    bool result = false;

    switch (type) {
        case TEST_ENGINE_COMPARE_SIGNED_INT:
            switch (mode) {
                case TEST_ENGINE_COMPARISON_EQ:
                    result = (ix == iy);
                    break;
                case TEST_ENGINE_COMPARISON_NE:
                    result = (ix != iy);
                    break;
                case TEST_ENGINE_COMPARISON_LT:
                    result = (ix < iy);
                    break;
                case TEST_ENGINE_COMPARISON_GT:
                    result = (ix > iy);
                    break;
                case TEST_ENGINE_COMPARISON_LE:
                    result = (ix <= iy);
                    break;
                case TEST_ENGINE_COMPARISON_GE:
                    result = (ix >= iy);
                    break;
                default:
                    mode   = TEST_ENGINE_COMPARISON_MODE_COUNT;
                    result = false;
            }
            break;
        case TEST_ENGINE_COMPARE_UNSIGNED_INT:
            switch (mode) {
                case TEST_ENGINE_COMPARISON_EQ:
                    result = (ux == uy);
                    break;
                case TEST_ENGINE_COMPARISON_NE:
                    result = (ux != uy);
                    break;
                case TEST_ENGINE_COMPARISON_LT:
                    result = (ux < uy);
                    break;
                case TEST_ENGINE_COMPARISON_GT:
                    result = (ux > uy);
                    break;
                case TEST_ENGINE_COMPARISON_LE:
                    result = (ux <= uy);
                    break;
                case TEST_ENGINE_COMPARISON_GE:
                    result = (ux >= uy);
                    break;
                default:
                    mode   = TEST_ENGINE_COMPARISON_MODE_COUNT;
                    result = false;
            }
            break;
        case TEST_ENGINE_COMPARE_FLOAT:
            switch (mode) {
                case TEST_ENGINE_COMPARISON_EQ:
                    result = (fx == fy);
                    break;
                case TEST_ENGINE_COMPARISON_NE:
                    result = (fx != fy);
                    break;
                case TEST_ENGINE_COMPARISON_LT:
                    result = (fx < fy);
                    break;
                case TEST_ENGINE_COMPARISON_GT:
                    result = (fx > fy);
                    break;
                case TEST_ENGINE_COMPARISON_LE:
                    result = (fx <= fy);
                    break;
                case TEST_ENGINE_COMPARISON_GE:
                    result = (fx >= fy);
                    break;
                default:
                    mode   = TEST_ENGINE_COMPARISON_MODE_COUNT;
                    result = false;
            }
            break;
        default:
            type   = TEST_ENGINE_COMPARE_SIGNED_INT;
            result = false;
    }

    if (result == false) {

        printf("%s:%ld\t", file_name, file_line);

        char check_message[TEST_ENGINE_CHECK_MESSAGE_LEN];
        memset(check_message, 0, TEST_ENGINE_CHECK_MESSAGE_LEN);
        switch (type) {
            case TEST_ENGINE_COMPARE_SIGNED_INT:
                snprintf(check_message,
                         TEST_ENGINE_CHECK_MESSAGE_LEN,
                         "%ld %s %ld",
                         ix,
                         TEST_ENGINE_COMPARISON_MODE_STRINGS[mode],
                         iy);
                break;
            case TEST_ENGINE_COMPARE_UNSIGNED_INT:
                snprintf(check_message,
                         TEST_ENGINE_CHECK_MESSAGE_LEN,
                         "%ld %s %ld",
                         ux,
                         TEST_ENGINE_COMPARISON_MODE_STRINGS[mode],
                         uy);
                break;
            case TEST_ENGINE_COMPARE_FLOAT:
                snprintf(check_message,
                         TEST_ENGINE_CHECK_MESSAGE_LEN,
                         "%10f %s %10f",
                         fx,
                         TEST_ENGINE_COMPARISON_MODE_STRINGS[mode],
                         fy);
                break;
            default:
                break;
        }
        printf("Check %s fails\n", check_message);
        fflush(stdout);
    }

    return result;
}