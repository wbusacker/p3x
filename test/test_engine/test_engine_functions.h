/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  server
Filename: test_engine_functions.h
Purpose:  CSC local function declarations
*/

#ifndef TEST_ENGINE_FUNCTIONS_H
#define TEST_ENGINE_FUNCTIONS_H
#include <test_engine.h>
#include <test_engine_data.h>

#endif
