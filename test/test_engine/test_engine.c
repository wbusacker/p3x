/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  server
Filename: test_engine.c
Purpose:  CSC data and initialization definitions
*/

#include <stdio.h>
#include <test_engine.h>
#include <test_engine_functions.h>

void init_test_engine(void* arg) {

    uint64_t pass_counter = 0;

    for (uint64_t i = 0; i < test_engine_auto_num_tests; i++) {

        printf("Executing test %s\n", test_engine_case_list[i].name);
        fflush(stdout);
        test_engine_case_list[i].function(&(test_engine_case_list[i].passed));

        if (test_engine_case_list[i].passed == true) {
            pass_counter++;
        }
    }

    printf("Executed %ld test cases, %ld passed\n", test_engine_auto_num_tests, pass_counter);

    for (uint64_t i = 0; i < test_engine_auto_num_tests; i++) {

        if (test_engine_case_list[i].passed == false) {
            printf("FAIL: %s\n", test_engine_case_list[i].name);
        }
    }
}

void teardown_test_engine(void* arg) {
}
