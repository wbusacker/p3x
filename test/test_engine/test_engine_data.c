/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  server
Filename: test_engine_data.c
Purpose:  CSC data definition
*/

#include <test_engine_data.h>

const char TEST_ENGINE_COMPARISON_MODE_STRINGS[TEST_ENGINE_COMPARISON_MODE_COUNT + 1]
                                              [TEST_ENGINE_COMPARISON_MODE_STRING_LEN] =
                                                {"==", "!=", "<", ">", "<=", ">=", ""};
