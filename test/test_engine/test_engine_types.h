/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  server
Filename: test_engine_types.h
Purpose:  CSC data types
*/

#ifndef TEST_ENGINE_TYPES_H
#define TEST_ENGINE_TYPES_H
#include <stdbool.h>
#include <test_engine_const.h>

typedef void (*Test_engine_case_func)(bool*);

struct Test_engine_case {
    Test_engine_case_func function;
    char                  name[TEST_ENGINE_MAX_CASE_NAME_LEN];
    bool                  passed;
};

#endif
