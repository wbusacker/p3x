/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  server
Filename: test_engine_data.h
Purpose:  CSC data declaration
*/

#ifndef TEST_ENGINE_DATA_H
#define TEST_ENGINE_DATA_H
#include <stdint.h>
#include <test_engine_const.h>
#include <test_engine_types.h>

extern const char TEST_ENGINE_COMPARISON_MODE_STRINGS[TEST_ENGINE_COMPARISON_MODE_COUNT + 1]
                                                     [TEST_ENGINE_COMPARISON_MODE_STRING_LEN];

extern uint64_t test_engine_passed_tests;

/* These values are filled in by the auto-generated C code */
extern uint64_t                test_engine_auto_num_tests;
extern struct Test_engine_case test_engine_case_list[];

#endif
