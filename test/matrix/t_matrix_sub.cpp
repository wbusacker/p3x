/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <matrix_functions.h>
}

TEST(MATRIX_SUB, WhenDouble4x4_ExpectCorrect) {

    matrix_4x4d A;
    A[matrix_4x4_index(0, 0)] = 1.0;
    A[matrix_4x4_index(0, 1)] = 2.0;
    A[matrix_4x4_index(0, 2)] = 3.0;
    A[matrix_4x4_index(0, 3)] = 4.0;

    A[matrix_4x4_index(1, 0)] = 5.0;
    A[matrix_4x4_index(1, 1)] = 6.0;
    A[matrix_4x4_index(1, 2)] = 7.0;
    A[matrix_4x4_index(1, 3)] = 8.0;

    A[matrix_4x4_index(2, 0)] = 9.0;
    A[matrix_4x4_index(2, 1)] = 10.0;
    A[matrix_4x4_index(2, 2)] = 11.0;
    A[matrix_4x4_index(2, 3)] = 12.0;

    A[matrix_4x4_index(3, 0)] = 13.0;
    A[matrix_4x4_index(3, 1)] = 14.0;
    A[matrix_4x4_index(3, 2)] = 15.0;
    A[matrix_4x4_index(3, 3)] = 16.0;

    matrix_4x4d B;
    B[matrix_4x4_index(0, 0)] = 3.0;
    B[matrix_4x4_index(0, 1)] = 6.0;
    B[matrix_4x4_index(0, 2)] = 9.0;
    B[matrix_4x4_index(0, 3)] = 12.0;

    B[matrix_4x4_index(1, 0)] = 15.0;
    B[matrix_4x4_index(1, 1)] = 18.0;
    B[matrix_4x4_index(1, 2)] = 21.0;
    B[matrix_4x4_index(1, 3)] = 24.0;

    B[matrix_4x4_index(2, 0)] = 27.0;
    B[matrix_4x4_index(2, 1)] = 30.0;
    B[matrix_4x4_index(2, 2)] = 33.0;
    B[matrix_4x4_index(2, 3)] = 36.0;

    B[matrix_4x4_index(3, 0)] = 39.0;
    B[matrix_4x4_index(3, 1)] = 42.0;
    B[matrix_4x4_index(3, 2)] = 45.0;
    B[matrix_4x4_index(3, 3)] = 48.0;

    matrix_4x4d C;

    matrix_4x4d_sub(B, A, C);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 0)], 2);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 1)], 4);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 2)], 6);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 3)], 8);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 0)], 10);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 1)], 12);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 2)], 14);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 3)], 16);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 0)], 18);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 1)], 20);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 2)], 22);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 3)], 24);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 0)], 26);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 1)], 28);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 2)], 30);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 3)], 32);
}

TEST(MATRIX_SUB, WhenDouble3x3_ExpectCorrect) {

    matrix_3x3d A;
    A[matrix_3x3_index(0, 0)] = 1.0;
    A[matrix_3x3_index(0, 1)] = 2.0;
    A[matrix_3x3_index(0, 2)] = 3.0;

    A[matrix_3x3_index(1, 0)] = 5.0;
    A[matrix_3x3_index(1, 1)] = 6.0;
    A[matrix_3x3_index(1, 2)] = 7.0;

    A[matrix_3x3_index(2, 0)] = 9.0;
    A[matrix_3x3_index(2, 1)] = 10.0;
    A[matrix_3x3_index(2, 2)] = 11.0;

    matrix_3x3d B;
    B[matrix_3x3_index(0, 0)] = 3.0;
    B[matrix_3x3_index(0, 1)] = 6.0;
    B[matrix_3x3_index(0, 2)] = 9.0;

    B[matrix_3x3_index(1, 0)] = 15.0;
    B[matrix_3x3_index(1, 1)] = 18.0;
    B[matrix_3x3_index(1, 2)] = 21.0;

    B[matrix_3x3_index(2, 0)] = 27.0;
    B[matrix_3x3_index(2, 1)] = 30.0;
    B[matrix_3x3_index(2, 2)] = 33.0;

    matrix_3x3d C;

    matrix_3x3d_sub(B, A, C);

    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 0)], 2);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 1)], 4);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 2)], 6);

    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 0)], 10);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 1)], 12);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 2)], 14);

    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 0)], 18);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 1)], 20);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 2)], 22);
}

TEST(MATRIX_SUB, WhenDouble2x2_ExpectCorrect) {

    matrix_2x2d A;
    A[matrix_2x2_index(0, 0)] = 1.0;
    A[matrix_2x2_index(0, 1)] = 2.0;

    A[matrix_2x2_index(1, 0)] = 5.0;
    A[matrix_2x2_index(1, 1)] = 6.0;

    matrix_2x2d B;
    B[matrix_2x2_index(0, 0)] = 3.0;
    B[matrix_2x2_index(0, 1)] = 6.0;

    B[matrix_2x2_index(1, 0)] = 15.0;
    B[matrix_2x2_index(1, 1)] = 18.0;

    matrix_2x2d C;

    matrix_2x2d_sub(B, A, C);

    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(0, 0)], 2);
    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(0, 1)], 4);

    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(1, 0)], 10);
    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(1, 1)], 12);
}

TEST(MATRIX_SUB, WhenFloat4x4_ExpectCorrect) {

    matrix_4x4f A;
    A[matrix_4x4_index(0, 0)] = 1.0;
    A[matrix_4x4_index(0, 1)] = 2.0;
    A[matrix_4x4_index(0, 2)] = 3.0;
    A[matrix_4x4_index(0, 3)] = 4.0;

    A[matrix_4x4_index(1, 0)] = 5.0;
    A[matrix_4x4_index(1, 1)] = 6.0;
    A[matrix_4x4_index(1, 2)] = 7.0;
    A[matrix_4x4_index(1, 3)] = 8.0;

    A[matrix_4x4_index(2, 0)] = 9.0;
    A[matrix_4x4_index(2, 1)] = 10.0;
    A[matrix_4x4_index(2, 2)] = 11.0;
    A[matrix_4x4_index(2, 3)] = 12.0;

    A[matrix_4x4_index(3, 0)] = 13.0;
    A[matrix_4x4_index(3, 1)] = 14.0;
    A[matrix_4x4_index(3, 2)] = 15.0;
    A[matrix_4x4_index(3, 3)] = 16.0;

    matrix_4x4f B;
    B[matrix_4x4_index(0, 0)] = 3.0;
    B[matrix_4x4_index(0, 1)] = 6.0;
    B[matrix_4x4_index(0, 2)] = 9.0;
    B[matrix_4x4_index(0, 3)] = 12.0;

    B[matrix_4x4_index(1, 0)] = 15.0;
    B[matrix_4x4_index(1, 1)] = 18.0;
    B[matrix_4x4_index(1, 2)] = 21.0;
    B[matrix_4x4_index(1, 3)] = 24.0;

    B[matrix_4x4_index(2, 0)] = 27.0;
    B[matrix_4x4_index(2, 1)] = 30.0;
    B[matrix_4x4_index(2, 2)] = 33.0;
    B[matrix_4x4_index(2, 3)] = 36.0;

    B[matrix_4x4_index(3, 0)] = 39.0;
    B[matrix_4x4_index(3, 1)] = 42.0;
    B[matrix_4x4_index(3, 2)] = 45.0;
    B[matrix_4x4_index(3, 3)] = 48.0;

    matrix_4x4f C;

    matrix_4x4f_sub(B, A, C);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 0)], 2);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 1)], 4);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 2)], 6);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 3)], 8);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 0)], 10);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 1)], 12);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 2)], 14);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 3)], 16);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 0)], 18);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 1)], 20);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 2)], 22);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 3)], 24);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 0)], 26);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 1)], 28);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 2)], 30);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 3)], 32);
}

TEST(MATRIX_SUB, WhenFloat3x3_ExpectCorrect) {

    matrix_3x3f A;
    A[matrix_3x3_index(0, 0)] = 1.0;
    A[matrix_3x3_index(0, 1)] = 2.0;
    A[matrix_3x3_index(0, 2)] = 3.0;

    A[matrix_3x3_index(1, 0)] = 5.0;
    A[matrix_3x3_index(1, 1)] = 6.0;
    A[matrix_3x3_index(1, 2)] = 7.0;

    A[matrix_3x3_index(2, 0)] = 9.0;
    A[matrix_3x3_index(2, 1)] = 10.0;
    A[matrix_3x3_index(2, 2)] = 11.0;

    matrix_3x3f B;
    B[matrix_3x3_index(0, 0)] = 3.0;
    B[matrix_3x3_index(0, 1)] = 6.0;
    B[matrix_3x3_index(0, 2)] = 9.0;

    B[matrix_3x3_index(1, 0)] = 15.0;
    B[matrix_3x3_index(1, 1)] = 18.0;
    B[matrix_3x3_index(1, 2)] = 21.0;

    B[matrix_3x3_index(2, 0)] = 27.0;
    B[matrix_3x3_index(2, 1)] = 30.0;
    B[matrix_3x3_index(2, 2)] = 33.0;

    matrix_3x3f C;

    matrix_3x3f_sub(B, A, C);

    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 0)], 2);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 1)], 4);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 2)], 6);

    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 0)], 10);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 1)], 12);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 2)], 14);

    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 0)], 18);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 1)], 20);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 2)], 22);
}

TEST(MATRIX_SUB, WhenFloat2x2_ExpectCorrect) {

    matrix_2x2f A;
    A[matrix_2x2_index(0, 0)] = 1.0;
    A[matrix_2x2_index(0, 1)] = 2.0;

    A[matrix_2x2_index(1, 0)] = 5.0;
    A[matrix_2x2_index(1, 1)] = 6.0;

    matrix_2x2f B;
    B[matrix_2x2_index(0, 0)] = 3.0;
    B[matrix_2x2_index(0, 1)] = 6.0;

    B[matrix_2x2_index(1, 0)] = 15.0;
    B[matrix_2x2_index(1, 1)] = 18.0;

    matrix_2x2f C;

    matrix_2x2f_sub(B, A, C);

    EXPECT_FLOAT_EQ(C[matrix_2x2_index(0, 0)], 2);
    EXPECT_FLOAT_EQ(C[matrix_2x2_index(0, 1)], 4);

    EXPECT_FLOAT_EQ(C[matrix_2x2_index(1, 0)], 10);
    EXPECT_FLOAT_EQ(C[matrix_2x2_index(1, 1)], 12);
}