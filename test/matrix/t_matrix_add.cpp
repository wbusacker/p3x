/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <matrix_functions.h>
}

TEST(MATRIX_ADD, WhenDouble4x4_ExpectCorrect) {

    //clang-format off
    matrix_4x4d A;
    A[matrix_4x4_index(0, 0)] = 1.0;
    A[matrix_4x4_index(0, 1)] = 2.0;
    A[matrix_4x4_index(0, 2)] = 3.0;
    A[matrix_4x4_index(0, 3)] = 4.0;

    A[matrix_4x4_index(1, 0)] = 5.0;
    A[matrix_4x4_index(1, 1)] = 6.0;
    A[matrix_4x4_index(1, 2)] = 7.0;
    A[matrix_4x4_index(1, 3)] = 8.0;

    A[matrix_4x4_index(2, 0)] = 9.0;
    A[matrix_4x4_index(2, 1)] = 10.0;
    A[matrix_4x4_index(2, 2)] = 11.0;
    A[matrix_4x4_index(2, 3)] = 12.0;

    A[matrix_4x4_index(3, 0)] = 13.0;
    A[matrix_4x4_index(3, 1)] = 14.0;
    A[matrix_4x4_index(3, 2)] = 15.0;
    A[matrix_4x4_index(3, 3)] = 16.0;

    matrix_4x4d B;
    B[matrix_4x4_index(0, 0)] = 3.0;
    B[matrix_4x4_index(0, 1)] = 6.0;
    B[matrix_4x4_index(0, 2)] = 9.0;
    B[matrix_4x4_index(0, 3)] = 12.0;

    B[matrix_4x4_index(1, 0)] = 15.0;
    B[matrix_4x4_index(1, 1)] = 18.0;
    B[matrix_4x4_index(1, 2)] = 21.0;
    B[matrix_4x4_index(1, 3)] = 24.0;

    B[matrix_4x4_index(2, 0)] = 27.0;
    B[matrix_4x4_index(2, 1)] = 30.0;
    B[matrix_4x4_index(2, 2)] = 33.0;
    B[matrix_4x4_index(2, 3)] = 36.0;

    B[matrix_4x4_index(3, 0)] = 39.0;
    B[matrix_4x4_index(3, 1)] = 42.0;
    B[matrix_4x4_index(3, 2)] = 45.0;
    B[matrix_4x4_index(3, 3)] = 48.0;
    //clang-format on

    matrix_4x4d C;

    matrix_4x4d_add(A, B, C);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 0)], 4);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 1)], 8);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 2)], 12);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 3)], 16);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 0)], 20);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 1)], 24);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 2)], 28);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 3)], 32);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 0)], 36);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 1)], 40);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 2)], 44);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 3)], 48);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 0)], 52);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 1)], 56);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 2)], 60);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 3)], 64);
}

TEST(MATRIX_ADD, WhenDouble3x3_ExpectCorrect) {

    //clang-format off
    matrix_3x3d A;
    A[matrix_3x3_index(0, 0)] = 1.0;
    A[matrix_3x3_index(0, 1)] = 2.0;
    A[matrix_3x3_index(0, 2)] = 3.0;

    A[matrix_3x3_index(1, 0)] = 5.0;
    A[matrix_3x3_index(1, 1)] = 6.0;
    A[matrix_3x3_index(1, 2)] = 7.0;

    A[matrix_3x3_index(2, 0)] = 9.0;
    A[matrix_3x3_index(2, 1)] = 10.0;
    A[matrix_3x3_index(2, 2)] = 11.0;

    matrix_3x3d B;
    B[matrix_3x3_index(0, 0)] = 3.0;
    B[matrix_3x3_index(0, 1)] = 6.0;
    B[matrix_3x3_index(0, 2)] = 9.0;

    B[matrix_3x3_index(1, 0)] = 15.0;
    B[matrix_3x3_index(1, 1)] = 18.0;
    B[matrix_3x3_index(1, 2)] = 21.0;

    B[matrix_3x3_index(2, 0)] = 27.0;
    B[matrix_3x3_index(2, 1)] = 30.0;
    B[matrix_3x3_index(2, 2)] = 33.0;
    //clang-format on

    matrix_3x3d C;

    matrix_3x3d_add(A, B, C);

    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 0)], 4);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 1)], 8);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 2)], 12);

    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 0)], 20);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 1)], 24);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 2)], 28);

    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 0)], 36);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 1)], 40);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 2)], 44);
}

TEST(MATRIX_ADD, WhenDouble2x2_ExpectCorrect) {

    //clang-format off
    matrix_2x2d A;
    A[matrix_2x2_index(0, 0)] = 1.0;
    A[matrix_2x2_index(0, 1)] = 2.0;

    A[matrix_2x2_index(1, 0)] = 5.0;
    A[matrix_2x2_index(1, 1)] = 6.0;

    matrix_2x2d B;
    B[matrix_2x2_index(0, 0)] = 3.0;
    B[matrix_2x2_index(0, 1)] = 6.0;

    B[matrix_2x2_index(1, 0)] = 15.0;
    B[matrix_2x2_index(1, 1)] = 18.0;
    //clang-format on

    matrix_2x2d C;

    matrix_2x2d_add(A, B, C);

    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(0, 0)], 4);
    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(0, 1)], 8);

    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(1, 0)], 20);
    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(1, 1)], 24);
}

TEST(MATRIX_ADD, WhenFloat4x4_ExpectCorrect) {

    //clang-format off
    matrix_4x4f A;
    A[matrix_4x4_index(0, 0)] = 1.0;
    A[matrix_4x4_index(0, 1)] = 2.0;
    A[matrix_4x4_index(0, 2)] = 3.0;
    A[matrix_4x4_index(0, 3)] = 4.0;

    A[matrix_4x4_index(1, 0)] = 5.0;
    A[matrix_4x4_index(1, 1)] = 6.0;
    A[matrix_4x4_index(1, 2)] = 7.0;
    A[matrix_4x4_index(1, 3)] = 8.0;

    A[matrix_4x4_index(2, 0)] = 9.0;
    A[matrix_4x4_index(2, 1)] = 10.0;
    A[matrix_4x4_index(2, 2)] = 11.0;
    A[matrix_4x4_index(2, 3)] = 12.0;

    A[matrix_4x4_index(3, 0)] = 13.0;
    A[matrix_4x4_index(3, 1)] = 14.0;
    A[matrix_4x4_index(3, 2)] = 15.0;
    A[matrix_4x4_index(3, 3)] = 16.0;

    matrix_4x4f B;
    B[matrix_4x4_index(0, 0)] = 3.0;
    B[matrix_4x4_index(0, 1)] = 6.0;
    B[matrix_4x4_index(0, 2)] = 9.0;
    B[matrix_4x4_index(0, 3)] = 12.0;

    B[matrix_4x4_index(1, 0)] = 15.0;
    B[matrix_4x4_index(1, 1)] = 18.0;
    B[matrix_4x4_index(1, 2)] = 21.0;
    B[matrix_4x4_index(1, 3)] = 24.0;

    B[matrix_4x4_index(2, 0)] = 27.0;
    B[matrix_4x4_index(2, 1)] = 30.0;
    B[matrix_4x4_index(2, 2)] = 33.0;
    B[matrix_4x4_index(2, 3)] = 36.0;

    B[matrix_4x4_index(3, 0)] = 39.0;
    B[matrix_4x4_index(3, 1)] = 42.0;
    B[matrix_4x4_index(3, 2)] = 45.0;
    B[matrix_4x4_index(3, 3)] = 48.0;
    //clang-format on

    matrix_4x4f C;

    matrix_4x4f_add(A, B, C);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 0)], 4);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 1)], 8);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 2)], 12);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 3)], 16);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 0)], 20);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 1)], 24);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 2)], 28);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 3)], 32);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 0)], 36);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 1)], 40);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 2)], 44);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 3)], 48);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 0)], 52);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 1)], 56);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 2)], 60);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 3)], 64);
}

TEST(MATRIX_ADD, WhenFloat3x3_ExpectCorrect) {

    //clang-format off
    matrix_3x3f A;
    A[matrix_3x3_index(0, 0)] = 1.0;
    A[matrix_3x3_index(0, 1)] = 2.0;
    A[matrix_3x3_index(0, 2)] = 3.0;

    A[matrix_3x3_index(1, 0)] = 5.0;
    A[matrix_3x3_index(1, 1)] = 6.0;
    A[matrix_3x3_index(1, 2)] = 7.0;

    A[matrix_3x3_index(2, 0)] = 9.0;
    A[matrix_3x3_index(2, 1)] = 10.0;
    A[matrix_3x3_index(2, 2)] = 11.0;

    matrix_3x3f B;
    B[matrix_3x3_index(0, 0)] = 3.0;
    B[matrix_3x3_index(0, 1)] = 6.0;
    B[matrix_3x3_index(0, 2)] = 9.0;

    B[matrix_3x3_index(1, 0)] = 15.0;
    B[matrix_3x3_index(1, 1)] = 18.0;
    B[matrix_3x3_index(1, 2)] = 21.0;

    B[matrix_3x3_index(2, 0)] = 27.0;
    B[matrix_3x3_index(2, 1)] = 30.0;
    B[matrix_3x3_index(2, 2)] = 33.0;
    //clang-format on

    matrix_3x3f C;

    matrix_3x3f_add(A, B, C);

    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 0)], 4);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 1)], 8);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 2)], 12);

    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 0)], 20);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 1)], 24);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 2)], 28);

    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 0)], 36);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 1)], 40);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 2)], 44);
}

TEST(MATRIX_ADD, WhenFloat2x2_ExpectCorrect) {

    //clang-format off
    matrix_2x2f A;
    A[matrix_2x2_index(0, 0)] = 1.0;
    A[matrix_2x2_index(0, 1)] = 2.0;

    A[matrix_2x2_index(1, 0)] = 5.0;
    A[matrix_2x2_index(1, 1)] = 6.0;

    matrix_2x2f B;
    B[matrix_2x2_index(0, 0)] = 3.0;
    B[matrix_2x2_index(0, 1)] = 6.0;

    B[matrix_2x2_index(1, 0)] = 15.0;
    B[matrix_2x2_index(1, 1)] = 18.0;
    //clang-format on

    matrix_2x2f C;

    matrix_2x2f_add(A, B, C);

    EXPECT_FLOAT_EQ(C[matrix_2x2_index(0, 0)], 4);
    EXPECT_FLOAT_EQ(C[matrix_2x2_index(0, 1)], 8);

    EXPECT_FLOAT_EQ(C[matrix_2x2_index(1, 0)], 20);
    EXPECT_FLOAT_EQ(C[matrix_2x2_index(1, 1)], 24);
}
