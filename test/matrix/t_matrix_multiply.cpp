/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <matrix_functions.h>
}

TEST(MATRIX_MULTIPLY, WhenDouble4x4_ExpectCorrect) {

    matrix_4x4d A;
    matrix_4x4d B;
    matrix_4x4d C;

    A[matrix_4x4_index(0, 0)] = 1.0;
    A[matrix_4x4_index(0, 1)] = 2.0;
    A[matrix_4x4_index(0, 2)] = 3.0;
    A[matrix_4x4_index(0, 3)] = 4.0;

    A[matrix_4x4_index(1, 0)] = 5.0;
    A[matrix_4x4_index(1, 1)] = 6.0;
    A[matrix_4x4_index(1, 2)] = 7.0;
    A[matrix_4x4_index(1, 3)] = 8.0;

    A[matrix_4x4_index(2, 0)] = 9.0;
    A[matrix_4x4_index(2, 1)] = 10.0;
    A[matrix_4x4_index(2, 2)] = 11.0;
    A[matrix_4x4_index(2, 3)] = 12.0;

    A[matrix_4x4_index(3, 0)] = 13.0;
    A[matrix_4x4_index(3, 1)] = 14.0;
    A[matrix_4x4_index(3, 2)] = 15.0;
    A[matrix_4x4_index(3, 3)] = 16.0;

    B[matrix_4x4_index(0, 0)] = 17.0;
    B[matrix_4x4_index(0, 1)] = 18.0;
    B[matrix_4x4_index(0, 2)] = 19.0;
    B[matrix_4x4_index(0, 3)] = 20.0;

    B[matrix_4x4_index(1, 0)] = 21.0;
    B[matrix_4x4_index(1, 1)] = 22.0;
    B[matrix_4x4_index(1, 2)] = 23.0;
    B[matrix_4x4_index(1, 3)] = 24.0;

    B[matrix_4x4_index(2, 0)] = 25.0;
    B[matrix_4x4_index(2, 1)] = 26.0;
    B[matrix_4x4_index(2, 2)] = 27.0;
    B[matrix_4x4_index(2, 3)] = 28.0;

    B[matrix_4x4_index(3, 0)] = 29.0;
    B[matrix_4x4_index(3, 1)] = 30.0;
    B[matrix_4x4_index(3, 2)] = 31.0;
    B[matrix_4x4_index(3, 3)] = 32.0;

    matrix_4x4d_multiply(A, B, C);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 0)], 250.0);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 1)], 260.0);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 2)], 270.0);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(0, 3)], 280.0);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 0)], 618);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 1)], 644);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 2)], 670);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(1, 3)], 696);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 0)], 986);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 1)], 1028);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 2)], 1070);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(2, 3)], 1112);

    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 0)], 1354);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 1)], 1412);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 2)], 1470);
    EXPECT_DOUBLE_EQ(C[matrix_4x4_index(3, 3)], 1528);
}

TEST(MATRIX_MULTIPLY, WhenDouble3x3_ExpectCorrect) {

    matrix_3x3d A;
    matrix_3x3d B;
    matrix_3x3d C;

    A[matrix_3x3_index(0, 0)] = 1.0;
    A[matrix_3x3_index(0, 1)] = 2.0;
    A[matrix_3x3_index(0, 2)] = 3.0;

    A[matrix_3x3_index(1, 0)] = 5.0;
    A[matrix_3x3_index(1, 1)] = 6.0;
    A[matrix_3x3_index(1, 2)] = 7.0;

    A[matrix_3x3_index(2, 0)] = 9.0;
    A[matrix_3x3_index(2, 1)] = 10.0;
    A[matrix_3x3_index(2, 2)] = 11.0;

    B[matrix_3x3_index(0, 0)] = 17.0;
    B[matrix_3x3_index(0, 1)] = 18.0;
    B[matrix_3x3_index(0, 2)] = 19.0;

    B[matrix_3x3_index(1, 0)] = 21.0;
    B[matrix_3x3_index(1, 1)] = 22.0;
    B[matrix_3x3_index(1, 2)] = 23.0;

    B[matrix_3x3_index(2, 0)] = 25.0;
    B[matrix_3x3_index(2, 1)] = 26.0;
    B[matrix_3x3_index(2, 2)] = 27.0;

    matrix_3x3d_multiply(A, B, C);

    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 0)], 134);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 1)], 140);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(0, 2)], 146);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 0)], 386);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 1)], 404);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(1, 2)], 422);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 0)], 638);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 1)], 668);
    EXPECT_DOUBLE_EQ(C[matrix_3x3_index(2, 2)], 698);
}

TEST(MATRIX_MULTIPLY, WhenDouble2x2_ExpectCorrect) {

    matrix_2x2d A;
    matrix_2x2d B;
    matrix_2x2d C;

    A[matrix_2x2_index(0, 0)] = 1.0;
    A[matrix_2x2_index(0, 1)] = 2.0;

    A[matrix_2x2_index(1, 0)] = 5.0;
    A[matrix_2x2_index(1, 1)] = 6.0;

    B[matrix_2x2_index(0, 0)] = 17.0;
    B[matrix_2x2_index(0, 1)] = 18.0;

    B[matrix_2x2_index(1, 0)] = 21.0;
    B[matrix_2x2_index(1, 1)] = 22.0;

    matrix_2x2d_multiply(A, B, C);

    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(0, 0)], 59);
    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(0, 1)], 62);

    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(1, 0)], 211);
    EXPECT_DOUBLE_EQ(C[matrix_2x2_index(1, 1)], 222);
}

TEST(MATRIX_MULTIPLY, WhenFloat4x4_ExpectCorrect) {

    matrix_4x4f A;
    matrix_4x4f B;
    matrix_4x4f C;

    A[matrix_4x4_index(0, 0)] = 1.0;
    A[matrix_4x4_index(0, 1)] = 2.0;
    A[matrix_4x4_index(0, 2)] = 3.0;
    A[matrix_4x4_index(0, 3)] = 4.0;

    A[matrix_4x4_index(1, 0)] = 5.0;
    A[matrix_4x4_index(1, 1)] = 6.0;
    A[matrix_4x4_index(1, 2)] = 7.0;
    A[matrix_4x4_index(1, 3)] = 8.0;

    A[matrix_4x4_index(2, 0)] = 9.0;
    A[matrix_4x4_index(2, 1)] = 10.0;
    A[matrix_4x4_index(2, 2)] = 11.0;
    A[matrix_4x4_index(2, 3)] = 12.0;

    A[matrix_4x4_index(3, 0)] = 13.0;
    A[matrix_4x4_index(3, 1)] = 14.0;
    A[matrix_4x4_index(3, 2)] = 15.0;
    A[matrix_4x4_index(3, 3)] = 16.0;

    B[matrix_4x4_index(0, 0)] = 17.0;
    B[matrix_4x4_index(0, 1)] = 18.0;
    B[matrix_4x4_index(0, 2)] = 19.0;
    B[matrix_4x4_index(0, 3)] = 20.0;

    B[matrix_4x4_index(1, 0)] = 21.0;
    B[matrix_4x4_index(1, 1)] = 22.0;
    B[matrix_4x4_index(1, 2)] = 23.0;
    B[matrix_4x4_index(1, 3)] = 24.0;

    B[matrix_4x4_index(2, 0)] = 25.0;
    B[matrix_4x4_index(2, 1)] = 26.0;
    B[matrix_4x4_index(2, 2)] = 27.0;
    B[matrix_4x4_index(2, 3)] = 28.0;

    B[matrix_4x4_index(3, 0)] = 29.0;
    B[matrix_4x4_index(3, 1)] = 30.0;
    B[matrix_4x4_index(3, 2)] = 31.0;
    B[matrix_4x4_index(3, 3)] = 32.0;

    matrix_4x4f_multiply(A, B, C);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 0)], 250.0);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 1)], 260.0);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 2)], 270.0);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(0, 3)], 280.0);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 0)], 618);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 1)], 644);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 2)], 670);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(1, 3)], 696);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 0)], 986);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 1)], 1028);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 2)], 1070);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(2, 3)], 1112);

    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 0)], 1354);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 1)], 1412);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 2)], 1470);
    EXPECT_FLOAT_EQ(C[matrix_4x4_index(3, 3)], 1528);
}

TEST(MATRIX_MULTIPLY, WhenFloat3x3_ExpectCorrect) {

    matrix_3x3f A;
    matrix_3x3f B;
    matrix_3x3f C;

    A[matrix_3x3_index(0, 0)] = 1.0;
    A[matrix_3x3_index(0, 1)] = 2.0;
    A[matrix_3x3_index(0, 2)] = 3.0;

    A[matrix_3x3_index(1, 0)] = 5.0;
    A[matrix_3x3_index(1, 1)] = 6.0;
    A[matrix_3x3_index(1, 2)] = 7.0;

    A[matrix_3x3_index(2, 0)] = 9.0;
    A[matrix_3x3_index(2, 1)] = 10.0;
    A[matrix_3x3_index(2, 2)] = 11.0;

    B[matrix_3x3_index(0, 0)] = 17.0;
    B[matrix_3x3_index(0, 1)] = 18.0;
    B[matrix_3x3_index(0, 2)] = 19.0;

    B[matrix_3x3_index(1, 0)] = 21.0;
    B[matrix_3x3_index(1, 1)] = 22.0;
    B[matrix_3x3_index(1, 2)] = 23.0;

    B[matrix_3x3_index(2, 0)] = 25.0;
    B[matrix_3x3_index(2, 1)] = 26.0;
    B[matrix_3x3_index(2, 2)] = 27.0;

    matrix_3x3f_multiply(A, B, C);

    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 0)], 134);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 1)], 140);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(0, 2)], 146);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 0)], 386);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 1)], 404);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(1, 2)], 422);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 0)], 638);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 1)], 668);
    EXPECT_FLOAT_EQ(C[matrix_3x3_index(2, 2)], 698);
}

TEST(MATRIX_MULTIPLY, WhenFloat2x2_ExpectCorrect) {

    matrix_2x2f A;
    matrix_2x2f B;
    matrix_2x2f C;

    A[matrix_2x2_index(0, 0)] = 1.0;
    A[matrix_2x2_index(0, 1)] = 2.0;

    A[matrix_2x2_index(1, 0)] = 5.0;
    A[matrix_2x2_index(1, 1)] = 6.0;

    B[matrix_2x2_index(0, 0)] = 17.0;
    B[matrix_2x2_index(0, 1)] = 18.0;

    B[matrix_2x2_index(1, 0)] = 21.0;
    B[matrix_2x2_index(1, 1)] = 22.0;

    matrix_2x2f_multiply(A, B, C);

    EXPECT_FLOAT_EQ(C[matrix_2x2_index(0, 0)], 59);
    EXPECT_FLOAT_EQ(C[matrix_2x2_index(0, 1)], 62);

    EXPECT_FLOAT_EQ(C[matrix_2x2_index(1, 0)], 211);
    EXPECT_FLOAT_EQ(C[matrix_2x2_index(1, 1)], 222);
}
