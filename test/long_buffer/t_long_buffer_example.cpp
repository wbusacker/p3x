/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <long_buffer_functions.h>
}

const char* const TEST_STRING = "beep";

TEST(Long_buffer, single_fragment_test) {

    struct Long_buffer* data = long_buffer_build();

    long_buffer_append(data, reinterpret_cast<const uint8_t*>(TEST_STRING), 5);

    uint8_t out_data[1024];
    long_buffer_copy_out(data, out_data, 1024);

    EXPECT_EQ(strcmp(TEST_STRING, reinterpret_cast<const char*>(out_data)), 0);

    long_buffer_delete(data);
}

TEST(Long_buffer, multiple_fragment_test) {

    struct Long_buffer* data = long_buffer_build();

    const uint64_t TEST_DATA_LEN = 1024 * 1024;
    const uint64_t OUT_DATA_LEN  = TEST_DATA_LEN + 1024;

    uint8_t* in_data_chunk  = (uint8_t*)malloc(TEST_DATA_LEN);
    uint8_t* out_data_chunk = (uint8_t*)malloc(OUT_DATA_LEN);
    for (uint64_t i = 0; i < TEST_DATA_LEN; i++) {
        in_data_chunk[i] = (i % 256);
    }

    memset(out_data_chunk, 0x77, OUT_DATA_LEN);

    long_buffer_append(data, in_data_chunk, TEST_DATA_LEN);
    long_buffer_copy_out(data, out_data_chunk, OUT_DATA_LEN);

    for (uint64_t i = 0; i < OUT_DATA_LEN; i++) {
        if (i < TEST_DATA_LEN) {
            EXPECT_EQ(out_data_chunk[i], i % 256);
            if (out_data_chunk[i] != (i % 256)) {
                printf("Failure");
            }
        } else {
            EXPECT_EQ(out_data_chunk[i], 0x77);
        }
    }

    EXPECT_EQ(data->total_data_len, TEST_DATA_LEN);

    long_buffer_delete(data);
    free(in_data_chunk);
    free(out_data_chunk);
}