/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: uptime_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <uptime_functions.h>
}
