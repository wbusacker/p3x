/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: t_tasking.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <tasking.h>
}

const uint8_t MAX_NUM_TASK_FREQUENCIES = 8;

volatile uint16_t test_duration = 0;

uint64_t t_tasking_dummy_function(uint64_t cycle_count, uint64_t mode, void* arg) {

    uint64_t return_code = 0;

    if (cycle_count >= test_duration) {
        return_code = 1;
    } else {

        uint64_t* true_arg = (uint64_t*)arg;

        *true_arg = *true_arg + 1;
    }

    return return_code;
}

class TASKING_CORRECT_FREQUENCY_EXECUTION : public ::testing::Test {

    public:
    TASKING_CORRECT_FREQUENCY_EXECUTION() { }

    void SetUp() {
        init_tasking();

        for (uint8_t i = 0; i < MAX_NUM_TASK_FREQUENCIES; i++) {
            execution_counts[i] = 0;
        }
    }

    void TearDown() { teardown_tasking(); }

    uint64_t execution_counts[MAX_NUM_TASK_FREQUENCIES];
};

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_50_HZ) {

    test_duration = 50;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 50);
}

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_10_HZ) {

    test_duration = 50;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_10_HZ, .arguments = execution_counts + 1},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 50);
    EXPECT_EQ(execution_counts[1], 10);
}

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_1_HZ) {

    test_duration = 100;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_10_HZ, .arguments = execution_counts + 1},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_HZ, .arguments = execution_counts + 2},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 100);
    EXPECT_EQ(execution_counts[1], 20);
    EXPECT_EQ(execution_counts[2], 2);
}

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_1_2_HZ) {

    test_duration = 200;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_10_HZ, .arguments = execution_counts + 1},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_HZ, .arguments = execution_counts + 2},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_2_HZ, .arguments = execution_counts + 3},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 200);
    EXPECT_EQ(execution_counts[1], 40);
    EXPECT_EQ(execution_counts[2], 4);
    EXPECT_EQ(execution_counts[3], 2);
}

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_1_10_HZ) {

    test_duration = 1000;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_10_HZ, .arguments = execution_counts + 1},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_HZ, .arguments = execution_counts + 2},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_2_HZ, .arguments = execution_counts + 3},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_10_HZ, .arguments = execution_counts + 4},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 1000);
    EXPECT_EQ(execution_counts[1], 200);
    EXPECT_EQ(execution_counts[2], 20);
    EXPECT_EQ(execution_counts[3], 10);
    EXPECT_EQ(execution_counts[4], 2);
}

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_1_30_HZ) {

    test_duration = 3000;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_10_HZ, .arguments = execution_counts + 1},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_HZ, .arguments = execution_counts + 2},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_2_HZ, .arguments = execution_counts + 3},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_10_HZ, .arguments = execution_counts + 4},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_30_HZ, .arguments = execution_counts + 5},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 3000);
    EXPECT_EQ(execution_counts[1], 600);
    EXPECT_EQ(execution_counts[2], 60);
    EXPECT_EQ(execution_counts[3], 30);
    EXPECT_EQ(execution_counts[4], 6);
    EXPECT_EQ(execution_counts[5], 2);
}

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_1_60_HZ) {

    test_duration = 3000;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_10_HZ, .arguments = execution_counts + 1},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_HZ, .arguments = execution_counts + 2},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_2_HZ, .arguments = execution_counts + 3},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_10_HZ, .arguments = execution_counts + 4},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_30_HZ, .arguments = execution_counts + 5},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_60_HZ, .arguments = execution_counts + 6},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 3000);
    EXPECT_EQ(execution_counts[1], 600);
    EXPECT_EQ(execution_counts[2], 60);
    EXPECT_EQ(execution_counts[3], 30);
    EXPECT_EQ(execution_counts[4], 6);
    EXPECT_EQ(execution_counts[5], 2);
    EXPECT_EQ(execution_counts[6], 1);
}

TEST_F(TASKING_CORRECT_FREQUENCY_EXECUTION, CORRECT_1_300_HZ) {

    test_duration = 15000;

    struct Tasking_entry entries[] = {
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_50_HZ, .arguments = execution_counts + 0},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_10_HZ, .arguments = execution_counts + 1},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_HZ, .arguments = execution_counts + 2},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_2_HZ, .arguments = execution_counts + 3},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_10_HZ, .arguments = execution_counts + 4},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_30_HZ, .arguments = execution_counts + 5},
      {.function = t_tasking_dummy_function, .frequency = TASKING_FREQUENCY_1_60_HZ, .arguments = execution_counts + 6},
      {.function  = t_tasking_dummy_function,
       .frequency = TASKING_FREQUENCY_1_300_HZ,
       .arguments = execution_counts + 7},
    };

    tasking_cycle(entries, sizeof(entries) / sizeof(struct Tasking_entry));

    EXPECT_EQ(execution_counts[0], 15000);
    EXPECT_EQ(execution_counts[1], 3000);
    EXPECT_EQ(execution_counts[2], 300);
    EXPECT_EQ(execution_counts[3], 150);
    EXPECT_EQ(execution_counts[4], 30);
    EXPECT_EQ(execution_counts[5], 10);
    EXPECT_EQ(execution_counts[6], 5);
    EXPECT_EQ(execution_counts[7], 1);
}