/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <json_functions.h>
}

const char* const TEST_JSON_STRING =
  "{\"key_object\":{\"key_string\":\"hi\"},\"key_array\":[\"string\",\"string1\"],\"key_number\":1.1}";

class JSON_Object_export : public ::testing::Test {

    public:
    JSON_Object_export() { }

    void SetUp() {
        uint8_t temp_string[135];
        memcpy(temp_string, TEST_JSON_STRING, 135);
        object = json_import_text_object(temp_string, 135);
    }

    void TearDown() { json_delete_object(object); }

    struct JSON_Object* object;
    struct Long_buffer* buffer;
};

TEST_F(JSON_Object_export, clean_export) {

    char* out_data = json_export_text_object(object);

    printf("%s\n", out_data);

    EXPECT_TRUE(true);
}