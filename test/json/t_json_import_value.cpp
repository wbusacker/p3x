/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: t_json_import_value.cpp
Purpose:  Checks all conditions for value importing
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <json_functions.h>
}

const uint8_t MESSAGE_LEN = 64;

class JSON_IMPORT_VALUE_FIXTURE : public ::testing::Test {

    public:
    JSON_IMPORT_VALUE_FIXTURE() { }

    struct JSON_Value value;
};

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportString) {

    uint8_t message[] = "\"hello world\"";

    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(value.string.string),
                      value.string.string_length),
              0);
    ASSERT_EQ(value.string.string_length, 11);
    ASSERT_EQ(value.datatype, JSON_STRING);
}

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportNumber) {

    uint8_t message[] = " 10.0 ";

    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_DOUBLE_EQ(10.0, value.number);
    ASSERT_EQ(value.datatype, JSON_NUMBER);
}

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportObject) {

    uint8_t message[]       = "{ \"key\" : 10.0 }";
    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);
    ASSERT_EQ(value.datatype, JSON_OBJECT);

    ASSERT_EQ(value.object.num_elements, 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(value.object.elements[0].key.string), "key", 3), 0);
    ASSERT_DOUBLE_EQ(value.object.elements[0].value.number, 10.0);
}

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportArray) {

    uint8_t message[]       = "[ \"key\" , 10.0 ]";
    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);
    ASSERT_EQ(value.datatype, JSON_ARRAY);

    ASSERT_EQ(value.array.num_values, 2);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(value.array.values[0].string.string), "key", 3), 0);
    ASSERT_DOUBLE_EQ(value.array.values[1].number, 10.0);
}

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportBoolean_True) {

    uint8_t message[] = "true";

    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_TRUE(value.boolean);
    ASSERT_EQ(value.datatype, JSON_BOOL);
}

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportBoolean_False) {

    uint8_t message[] = "false";

    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_FALSE(value.boolean);
    ASSERT_EQ(value.datatype, JSON_BOOL);
}

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportNull) {

    uint8_t message[] = "null";

    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_TRUE(value.is_null);
    ASSERT_EQ(value.datatype, JSON_NULL);
}

TEST_F(JSON_IMPORT_VALUE_FIXTURE, ImportGarbage) {

    uint8_t message[] = "garbage";

    int32_t read_characters = json_import_value(message, &value);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(value.datatype, JSON_ERROR);
}