/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <json_functions.h>
}

class JSON_String_export : public ::testing::Test {

    public:
    JSON_String_export() { }

    void SetUp() {
        test_string = json_build_string(TEST_STRING_CHAR);
        buffer      = long_buffer_build();
    }

    void TearDown() {
        json_delete_string(test_string);
        free(test_string);
        long_buffer_delete(buffer);
    }

    const char* const TEST_STRING_CHAR = "what is up\\my\\chick\\peas\n";

    uint8_t             buffer_len;
    struct JSON_String* test_string;
    struct Long_buffer* buffer;
};

TEST_F(JSON_String_export, clean_export) {

    json_export_string(buffer, test_string);

    uint8_t* continuous_buffer = (uint8_t*)malloc(buffer->total_data_len + 128);
    memset(continuous_buffer, 0, buffer->total_data_len + 128);

    long_buffer_copy_out(buffer, continuous_buffer, buffer->total_data_len + 128);

    printf("%s\n", continuous_buffer);

    // EXPECT_EQ(strncmp(TEST_STRING_CHAR, reinterpret_cast<char*>(continuous_buffer), buffer_len), 0);

    free(continuous_buffer);
    EXPECT_TRUE(true);
}