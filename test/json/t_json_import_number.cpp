/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: t_json_import_string.cpp
Purpose:  Checks all conditions for string importing
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <json_functions.h>
}

const uint8_t MESSAGE_LEN = 64;

class JSON_IMPORT_NUMBER_BASIC_CONVERSION :
    public ::testing::TestWithParam<std::tuple<const char* const, double, int32_t>> {

    public:
    JSON_IMPORT_NUMBER_BASIC_CONVERSION() { }

    void SetUp(void) {

        memset(message, 0, MESSAGE_LEN);

        strncpy(reinterpret_cast<char*>(message), std::get<0>(GetParam()), MESSAGE_LEN);
        expected_value           = std::get<1>(GetParam());
        expected_characters_read = std::get<2>(GetParam());

        whitespace_end = 0;
        while (message[whitespace_end] != '\0') {
            whitespace_end++;
        }
    }

    uint8_t message[MESSAGE_LEN];
    double  expected_value;
    int32_t expected_characters_read;
    uint8_t whitespace_end;
};

TEST_P(JSON_IMPORT_NUMBER_BASIC_CONVERSION, WhenSpace_ExpectValidConversion) {

    double number = 0;

    uint8_t local_message[MESSAGE_LEN];
    memcpy(local_message, message, MESSAGE_LEN);

    local_message[whitespace_end]     = ' ';
    local_message[whitespace_end + 1] = '\0';

    ASSERT_EQ(json_import_number(local_message, &number), expected_characters_read);
    ASSERT_DOUBLE_EQ(number, expected_value);
}

TEST_P(JSON_IMPORT_NUMBER_BASIC_CONVERSION, WhenNewline_ExpectValidConversion) {

    double number = 0;

    uint8_t local_message[MESSAGE_LEN];
    memcpy(local_message, message, MESSAGE_LEN);

    local_message[whitespace_end]     = '\n';
    local_message[whitespace_end + 1] = '\0';

    ASSERT_EQ(json_import_number(local_message, &number), expected_characters_read);
    ASSERT_DOUBLE_EQ(number, expected_value);
}

TEST_P(JSON_IMPORT_NUMBER_BASIC_CONVERSION, WhenTab_ExpectValidConversion) {

    double number = 0;

    uint8_t local_message[MESSAGE_LEN];
    memcpy(local_message, message, MESSAGE_LEN);

    local_message[whitespace_end]     = '\t';
    local_message[whitespace_end + 1] = '\0';

    ASSERT_EQ(json_import_number(local_message, &number), expected_characters_read);
    ASSERT_DOUBLE_EQ(number, expected_value);
}

TEST_P(JSON_IMPORT_NUMBER_BASIC_CONVERSION, WhenReturn_ExpectValidConversion) {

    double number = 0;

    uint8_t local_message[MESSAGE_LEN];
    memcpy(local_message, message, MESSAGE_LEN);

    local_message[whitespace_end]     = '\r';
    local_message[whitespace_end + 1] = '\0';

    ASSERT_EQ(json_import_number(local_message, &number), expected_characters_read);
    ASSERT_DOUBLE_EQ(number, expected_value);
}

TEST_P(JSON_IMPORT_NUMBER_BASIC_CONVERSION, WhenComma_ExpectValidConversion) {

    double number = 0;

    uint8_t local_message[MESSAGE_LEN];
    memcpy(local_message, message, MESSAGE_LEN);

    local_message[whitespace_end]     = ',';
    local_message[whitespace_end + 1] = '\0';

    ASSERT_EQ(json_import_number(local_message, &number), expected_characters_read);
    ASSERT_DOUBLE_EQ(number, expected_value);
}

INSTANTIATE_TEST_SUITE_P(JSON_IMPORT_NUMBER_BASIC_CONVERSION,
                         JSON_IMPORT_NUMBER_BASIC_CONVERSION,
                         ::testing::Values(std::make_tuple("10 ", 10.0, 2),
                                           std::make_tuple("10.1 ", 10.1, 4),
                                           std::make_tuple("10e1 ", 100.0, 4),
                                           std::make_tuple("10.1e1 ", 101.0, 6),
                                           std::make_tuple("10e-1 ", 1.0, 5),
                                           std::make_tuple("10.1e-1 ", 1.010, 7),
                                           std::make_tuple("-10 ", -10.0, 3),
                                           std::make_tuple("-10.1 ", -10.1, 5),
                                           std::make_tuple("-10e1 ", -100.0, 5),
                                           std::make_tuple("-10.1e1 ", -101.0, 7),
                                           std::make_tuple("-10e-1 ", -1.0, 6),
                                           std::make_tuple("-10.1e-1 ", -1.010, 8),
                                           std::make_tuple("10E1 ", 100.0, 4),
                                           std::make_tuple("10.1E1 ", 101.0, 6),
                                           std::make_tuple("10E-1 ", 1.0, 5),
                                           std::make_tuple("10.1E-1 ", 1.010, 7),
                                           std::make_tuple("-10E1 ", -100.0, 5),
                                           std::make_tuple("-10.1E1 ", -101.0, 7),
                                           std::make_tuple("-10E-1 ", -1.0, 6),
                                           std::make_tuple("-10.1E-1 ", -1.010, 8)));

TEST(JSON_IMPORT_NUMBER, WhenBadInteger_ExpectFail) {

    double number = 0;

    uint8_t message[] = "010 ";

    ASSERT_EQ(json_import_number(message, &number), JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 1);
}

TEST(JSON_IMPORT_NUMBER, WhenBadFraction_ExpectFail) {

    double number = 0;

    uint8_t message[] = "1.a0 ";

    ASSERT_EQ(json_import_number(message, &number), JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 2);
}

TEST(JSON_IMPORT_NUMBER, WhenBadExponent_ExpectFail) {

    double number = 0;

    uint8_t message[] = "1.0ea ";

    ASSERT_EQ(json_import_number(message, &number), JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 4);
}