/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: t_json_import_text_array.cpp
Purpose:  Checks all conditions for value importing
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <json_functions.h>
}

const char JSON_EXAMPLE[] = "             \
[                                         \
  \"test_string\",                        \
  10.0,                                   \
  {                                       \
    \"lvl2_string\" : \"test_string2\",   \
    \"lvl2_number\" : 20.0e1,             \
    \"lvl2_array\" : {                   \
      \"lvl3_string\" : \"test_string3\", \
      \"lvl3_number\" : 40.0e2            \
    },                                    \
    \"lvl2_array\" : [                    \
        \"array_string2\",                \
        80e3                              \
    ]                                     \
  },                                      \
  [                                       \
    \"array_string1\",                    \
    160e4                                 \
  ]                                       \
]                                         \
";

class JSON_TEXT_IMPORT_ARRAY_FIXTURE : public ::testing::Test {

    public:
    JSON_TEXT_IMPORT_ARRAY_FIXTURE() { }

    void SetUp() {

        message = reinterpret_cast<uint8_t*>(malloc(sizeof(JSON_EXAMPLE)));

        strcpy(reinterpret_cast<char*>(message), JSON_EXAMPLE);
    }

    struct JSON_Array* array;
    uint8_t*           message;
};

TEST_F(JSON_TEXT_IMPORT_ARRAY_FIXTURE, Expected_Access) {
    array = json_import_text_array(message, sizeof(JSON_EXAMPLE));

    ASSERT_EQ(array->num_values, 4);

    ASSERT_EQ(strncmp("test_string1", reinterpret_cast<char*>(array->values[0].string.string), 11), 0);
    ASSERT_DOUBLE_EQ(array->values[1].number, 10.0);

    ASSERT_EQ(strncmp("test_string2",
                      reinterpret_cast<char*>(array->values[2].object.elements[0].value.string.string),
                      11),
              0);
    ASSERT_DOUBLE_EQ(array->values[2].object.elements[1].value.number, 200.0);

    ASSERT_EQ(strncmp("test_string3",
                      reinterpret_cast<char*>(
                        array->values[2].object.elements[2].value.object.elements[0].value.string.string),
                      11),
              0);
    ASSERT_DOUBLE_EQ(array->values[2].object.elements[2].value.object.elements[1].value.number, 4000.0);

    ASSERT_EQ(strncmp("array_string2",
                      reinterpret_cast<char*>(array->values[2].object.elements[3].value.array.values[0].string.string),
                      12),
              0);
    ASSERT_DOUBLE_EQ(array->values[2].object.elements[3].value.array.values[1].number, 80000.0);

    ASSERT_EQ(strncmp("array_string1", reinterpret_cast<char*>(array->values[3].array.values[0].string.string), 12), 0);
    ASSERT_DOUBLE_EQ(array->values[3].array.values[1].number, 1600000.0);
}

TEST_F(JSON_TEXT_IMPORT_ARRAY_FIXTURE, Malformed_array) {

    message[0] = 'a';

    array = json_import_text_array(message, sizeof(JSON_EXAMPLE));

    EXPECT_EQ(reinterpret_cast<uint64_t>(array), NULL);
}