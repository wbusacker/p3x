/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: t_json_import_string.cpp
Purpose:  Checks all conditions for string importing
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <json_functions.h>
}

TEST(JSON_IMPORT_STRING, StandardText) {

    struct JSON_String string;

    uint8_t message[] = "\"hello world\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 11);
}

TEST(JSON_IMPORT_STRING, EscapedQuote) {

    struct JSON_String string;

    uint8_t message[] = "\"\\\"\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, EscapedBackslash) {

    struct JSON_String string;

    uint8_t message[] = "\"\\\\\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, EscapedForwardSlash) {

    struct JSON_String string;

    uint8_t message[] = "\"\\/\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, EscapedBackspace) {

    struct JSON_String string;

    uint8_t message[] = "\"\\b\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, EscapedFormFeed) {

    struct JSON_String string;

    uint8_t message[] = "\"\\f\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, EscapedNewline) {

    struct JSON_String string;

    uint8_t message[] = "\"\\n\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, EscapedCarriageReturn) {

    struct JSON_String string;

    uint8_t message[] = "\"\\r\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, EscapedTab) {

    struct JSON_String string;

    uint8_t message[] = "\"\\t\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 1),
                      reinterpret_cast<char*>(string.string),
                      string.string_length),
              0);
    ASSERT_EQ(string.string_length, 1);
}

TEST(JSON_IMPORT_STRING, MissingLeadingQuote) {

    struct JSON_String string;

    uint8_t message[] = "hello world\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(reinterpret_cast<uint64_t>(string.string), NULL);
    ASSERT_EQ(string.string_length, 0);
}

TEST(JSON_IMPORT_STRING, UnescapedBackslash) {

    struct JSON_String string;

    uint8_t message[] = "\"hello \\world\"";

    int32_t read_characters = json_import_string(message, &string);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(reinterpret_cast<uint64_t>(string.string), NULL);
    ASSERT_EQ(string.string_length, 0);
}