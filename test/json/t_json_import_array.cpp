/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: t_json_import_value.cpp
Purpose:  Checks all conditions for value importing
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <json_functions.h>
}

class JSON_IMPORT_ARRAY_FIXTURE : public ::testing::Test {

    public:
    JSON_IMPORT_ARRAY_FIXTURE() { }

    struct JSON_Array array;
};

TEST_F(JSON_IMPORT_ARRAY_FIXTURE, ImportArray_Single) {

    uint8_t message[] = "[ \"pair\" ]";

    int32_t read_characters = json_import_array(message, &array);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(array.num_values, 1);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 3),
                      reinterpret_cast<char*>(array.values[0].string.string),
                      array.values[0].string.string_length),
              0);
}

TEST_F(JSON_IMPORT_ARRAY_FIXTURE, ImportArray_Multiple) {

    uint8_t message[] = "[\"pair\" , 10.0 ]";

    int32_t read_characters = json_import_array(message, &array);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(array.num_values, 2);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 2),
                      reinterpret_cast<char*>(array.values[0].string.string),
                      array.values[0].string.string_length),
              0);
    ASSERT_DOUBLE_EQ(array.values[1].number, 10.0);
}

TEST_F(JSON_IMPORT_ARRAY_FIXTURE, ImportArray_MalformedMultiple) {

    uint8_t message[] = "[\"pair\"  10.0 ]";

    int32_t read_characters = json_import_array(message, &array);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 9);
}

TEST_F(JSON_IMPORT_ARRAY_FIXTURE, ImportArray_MalformedValue) {

    uint8_t message[] = "[\"pair\" , 10+0 ]";

    int32_t read_characters = json_import_array(message, &array);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 12);
}

// TEST_F(JSON_IMPORT_ARRAY_FIXTURE, Unclosed_Object) {

//     uint8_t message[] = "{ \"title\" : \"pair\" , \"title2\" : 10.0 ,";

//     int32_t read_characters = json_import_array(message, &array);

//     printf("%ld\n", message - json_read_failure_index);

//     ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
//     ASSERT_EQ(json_read_failure_index, message);
// }

// TEST_F(JSON_IMPORT_ARRAY_FIXTURE, Unopened_Object) {

//     uint8_t message[] = " \"title\" : \"pair\" , \"title2\" : 10.0 }";

//     int32_t read_characters = json_import_array(message, &array);

//     ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
//     ASSERT_EQ(json_read_failure_index, message);
// }
