/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: t_json_import_value.cpp
Purpose:  Checks all conditions for value importing
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <json_functions.h>
}

class JSON_IMPORT_OBJECT_FIXTURE : public ::testing::Test {

    public:
    JSON_IMPORT_OBJECT_FIXTURE() { }

    struct JSON_Object object;
};

TEST_F(JSON_IMPORT_OBJECT_FIXTURE, ImportKeypair_Single) {

    uint8_t message[] = "{ \"title\" : \"pair\" }";

    int32_t read_characters = json_import_object(message, &object);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(object.num_elements, 1);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 13),
                      reinterpret_cast<char*>(object.elements[0].value.string.string),
                      object.elements[0].value.string.string_length),
              0);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 3),
                      reinterpret_cast<char*>(object.elements[0].key.string),
                      object.elements[0].key.string_length),
              0);
}

TEST_F(JSON_IMPORT_OBJECT_FIXTURE, ImportKeypair_Single2) {

    uint8_t message[] = "{ \"title\" : false }";

    int32_t read_characters = json_import_object(message, &object);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(object.num_elements, 1);
    ASSERT_FALSE(object.elements[0].value.boolean);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 3),
                      reinterpret_cast<char*>(object.elements[0].key.string),
                      object.elements[0].key.string_length),
              0);
}

TEST_F(JSON_IMPORT_OBJECT_FIXTURE, ImportKeypair_Multiple) {

    uint8_t message[] = "{ \"title\" : \"pair\" , \"title2\" : 10.0 }";

    int32_t read_characters = json_import_object(message, &object);

    ASSERT_EQ(read_characters, (sizeof(message) / sizeof(uint8_t)) - 1);

    ASSERT_EQ(object.num_elements, 2);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 13),
                      reinterpret_cast<char*>(object.elements[0].value.string.string),
                      object.elements[0].value.string.string_length),
              0);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 3),
                      reinterpret_cast<char*>(object.elements[0].key.string),
                      object.elements[0].key.string_length),
              0);
    ASSERT_DOUBLE_EQ(object.elements[1].value.number, 10.0);
    ASSERT_EQ(strncmp(reinterpret_cast<char*>(message + 22),
                      reinterpret_cast<char*>(object.elements[1].key.string),
                      object.elements[1].key.string_length),
              0);
}

TEST_F(JSON_IMPORT_OBJECT_FIXTURE, ImportKeypair_MalformedMultiple) {

    uint8_t message[] = "{ \"title\" : \"pair\"   \"title2\" : 10.0 }";

    int32_t read_characters = json_import_object(message, &object);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 21);
}

TEST_F(JSON_IMPORT_OBJECT_FIXTURE, ImportKeypair_MalformedKeypair) {

    uint8_t message[] = "{ \"title\"   \"pair\" , \"title2\" : 10.0 }";

    int32_t read_characters = json_import_object(message, &object);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 12);
}

TEST_F(JSON_IMPORT_OBJECT_FIXTURE, Unclosed_Object) {

    uint8_t message[] = "{ \"title\" : \"pair\" , \"title2\" : 10.0 ,";

    int32_t read_characters = json_import_object(message, &object);

    printf("%ld\n", message - json_read_failure_index);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message + 38);
}

TEST_F(JSON_IMPORT_OBJECT_FIXTURE, Unopened_Object) {

    uint8_t message[] = " \"title\" : \"pair\" , \"title2\" : 10.0 }";

    int32_t read_characters = json_import_object(message, &object);

    ASSERT_EQ(read_characters, JSON_BAD_IMPORT);
    ASSERT_EQ(json_read_failure_index, message);
}
