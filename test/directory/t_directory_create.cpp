/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <directory_functions.h>
}

TEST(Directory_Create, make_directory) {
    char path[] = "sup/my/chickpeas";
    EXPECT_TRUE(directory_create(path));
}

TEST(Directory_Create, make_existing_directory) {
    char path[] = "sup/my/chickpeas";
    EXPECT_TRUE(directory_create(path));
    EXPECT_TRUE(directory_create(path));
}