/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <directory_functions.h>
}

TEST(Directory_Get_current_name, expect_something) {
    /* We can't really expect a whole lot here without using the exact same mechanisms
        so at best we can just assume that we get _something_ back with a null terminated
        string
    */
    char path[MAX_LEN_FILESYSTEM_NAME];
    memset(path, 0x77, MAX_LEN_FILESYSTEM_NAME);

    uint32_t path_len = directory_get_current_name(path);

    EXPECT_EQ(path[path_len], 0);

    for (uint32_t i = path_len + 1; i < MAX_LEN_FILESYSTEM_NAME; i++) {
        EXPECT_EQ(path[i], 0x77);
    }
}