/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_example.cpp
Purpose:  CSC gtest module
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

extern "C" {
#include <log_functions.h>
}

TEST(LOG, GenAllLogLevels) {
    init_log("Debug Test");

    log_set_global_level(LOG_LEVEL_DEBUG);

    LOG_ALWAYS("Always message\n");
    LOG_CRITICAL("Critical Message\n");
    LOG_ERROR("Error Message\n");
    LOG_WARNING("Warning Message\n");
    LOG_INFO("Info Message\n");
    LOG_DEBUG("Debug Message\n");

    teardown_log();
}
