#!/usr/bin/python3
import os
from pickle import OBJ

OBJECT_STORAGE = "bld\\win_obj"
COMPILED_NAME = "p3x.exe"

MSVC_INSTALL_DIR = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\"
SHELL_VAR_DIR = "Auxiliary\\Build\\vcvars64.bat"

GLFW_INSTALL_DIR = "C:\\glfw\\glfw-3.3.4.bin.WIN64\\glfw-3.3.4.bin.WIN64"
GLEW_INSTALL_DIR = "C:\\glew\\2.2.0\\x64"

COMPILER_FLAGS = "/EHsc /O2 /MD /Fo%s\\ /c" % OBJECT_STORAGE
LINKER_OPTIONS = "/OUT:%s" % COMPILED_NAME
LINKER_LIBRARIES = [
    "%s\\lib-vc2017\\glfw3.lib" % GLFW_INSTALL_DIR,
    "%s\\glew32.lib" % GLEW_INSTALL_DIR,
    "opengl32.lib",
    "kernel32.lib",
    "user32.lib",
    "gdi32.lib",
    "winspool.lib",
    "comdlg32.lib",
    "advapi32.lib",
    "shell32.lib",
    "ole32.lib",
    "oleaut32.lib",
    "uuid.lib",
    "odbc32.lib",
    "odbccp32.lib"
]
# Find all of the files to compile

source_files = []
source_file_names = []
directories = []
for root, dir, filenames in os.walk("src/"):
    for file in filenames:
        if ".c" in file:
            source_files.append(os.path.join(root, file).replace("/", "\\"))
            source_file_names.append(file.split(".")[0])
    for d in dir:
        directories.append(os.path.join(root,d).replace("/", "\\"))

include_arguments = ""
for dir in directories:
    include_arguments += "/I %s " % dir

include_arguments += " /I %s\\include " % GLFW_INSTALL_DIR

with open("build_win.bat", "w") as fh:
    # Configure the build environment, but only if it hasn't been already
    fh.write("echo OFF\n")
    fh.write("if \"%__VCVARSALL_HOST_ARCH%\" == \"\" (\n")
    fh.write("CALL " + MSVC_INSTALL_DIR + SHELL_VAR_DIR + "\"")
    fh.write("\n)")

    fh.write("\n")

    fh.write("mkdir %s\n\n" % OBJECT_STORAGE)

    fh.write("del %s\n" % COMPILED_NAME)

    # issue all of the compile commands
    for file in source_files:
        fh.write("cl %s %s %s \n" % (COMPILER_FLAGS, file, include_arguments))
        fh.write("if %ERRORLEVEL% neq 0 exit /b\n")

    # link the program together
    fh.write("LINK ")
    for file in source_file_names:
        fh.write("%s\\%s.obj " % (OBJECT_STORAGE, file))
    for lib in LINKER_LIBRARIES:
        fh.write("%s " % lib)
    fh.write(LINKER_OPTIONS)
    fh.write("\n")
    fh.write("if %ERRORLEVEL% neq 0 exit /b\n")

    # Link everything but main into a DLL
    fh.write("LINK ")
    for file in source_file_names:
        if file != "main":            
            fh.write("%s\\%s.obj " % (OBJECT_STORAGE, file))
    fh.write("/DLL /OUT:install\\lib\\p3x.dll\n")
    fh.write("if %ERRORLEVEL% neq 0 exit /b\n")

    # Link everything but main into a Static Library
    fh.write("LIB ")
    for file in source_file_names:
        if file != "main":            
            fh.write("%s\\%s.obj " % (OBJECT_STORAGE, file))
    fh.write("/OUT:install\\lib\\p3x.lib\n")
    fh.write("if %ERRORLEVEL% neq 0 exit /b\n")

    # Install the updated library
    fh.write("XCOPY install\\* C:\\p3X\\ /E /F /Y ")


