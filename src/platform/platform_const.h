/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: platform_const.h
Purpose:  CSC constants
*/

#ifndef PLATFORM_CONST_H
#define PLATFORM_CONST_H

#if defined(_MSC_VER)
    #define PLATFORM_IS_NT
#elif defined(__GNUC__)
    #define PLATFORM_IS_POSIX
#endif

#endif
