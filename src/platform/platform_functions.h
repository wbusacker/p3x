/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: platform_functions.h
Purpose:  CSC local function declarations
*/

#ifndef PLATFORM_FUNCTIONS_H
#define PLATFORM_FUNCTIONS_H
#include <platform.h>
#include <platform_data.h>

#endif
