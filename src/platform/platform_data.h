/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: platform_data.h
Purpose:  CSC data declaration
*/

#ifndef PLATFORM_DATA_H
#define PLATFORM_DATA_H
#include <platform_const.h>
#include <platform_types.h>

#endif
