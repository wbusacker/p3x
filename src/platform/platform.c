/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: platform.c
Purpose:  CSC data and initialization definitions
*/

#include <platform.h>
#include <platform_data.h>

uint8_t init_platform(void) {
}

uint8_t teardown_platform(void) {
}
