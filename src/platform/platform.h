/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: platform.h
Purpose:  External CSC Header
*/

#ifndef PLATFORM_H
#define PLATFORM_H
#include <platform_const.h>
#include <platform_types.h>
#include <stdint.h>

uint8_t init_platform(void);
uint8_t teardown_platform(void);

#endif
