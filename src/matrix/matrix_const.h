/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix_const.h
Purpose:  CSC constants
*/

#ifndef MATRIX_CONST_H
#define MATRIX_CONST_H

#define MATRIX_DIM_4 (4)
#define MATRIX_DIM_3 (3)
#define MATRIX_DIM_2 (2)

#define MATRIX_4X4_LEN (MATRIX_DIM_4 * MATRIX_DIM_4)
#define MATRIX_3X3_LEN (MATRIX_DIM_3 * MATRIX_DIM_3)
#define MATRIX_2X2_LEN (MATRIX_DIM_2 * MATRIX_DIM_2)

#endif
