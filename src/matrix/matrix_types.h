/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix_types.h
Purpose:  CSC data types
*/

#ifndef MATRIX_TYPES_H
#define MATRIX_TYPES_H
#include <matrix_const.h>

typedef double matrix_4x4d[MATRIX_4X4_LEN];
typedef double matrix_3x3d[MATRIX_3X3_LEN];
typedef double matrix_2x2d[MATRIX_2X2_LEN];

typedef float matrix_4x4f[MATRIX_4X4_LEN];
typedef float matrix_3x3f[MATRIX_3X3_LEN];
typedef float matrix_2x2f[MATRIX_2X2_LEN];

#endif
