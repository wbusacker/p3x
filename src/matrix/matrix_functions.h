/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix_functions.h
Purpose:  CSC local function declarations
*/

#ifndef MATRIX_FUNCTIONS_H
#define MATRIX_FUNCTIONS_H
#include <matrix.h>
#include <matrix_data.h>

#endif
