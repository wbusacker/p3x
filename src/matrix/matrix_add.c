/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix.c
Purpose:  CSC data and initialization definitions
*/

#include <matrix_functions.h>

double* matrix_d_add(double* lhs,
                     double* rhs,
                     double* out,
                     uint8_t lhs_row,
                     uint8_t lhs_col,
                     uint8_t rhs_row,
                     uint8_t rhs_col) {

    if ((lhs_col == rhs_col) && (lhs_row == rhs_row)) {

        for (uint8_t r = 0; r < lhs_row; r++) {
            for (uint8_t c = 0; c < rhs_col; c++) {
                out[matrix_index(r, c, lhs_row)] = lhs[matrix_index(r, c, lhs_row)] + rhs[matrix_index(r, c, lhs_row)];
            }
        }
    }

    return out;
}

float*
matrix_f_add(float* lhs, float* rhs, float* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_row, uint8_t rhs_col) {

    if ((lhs_col == rhs_col) && (lhs_row == rhs_row)) {

        for (uint8_t r = 0; r < lhs_row; r++) {
            for (uint8_t c = 0; c < rhs_col; c++) {
                out[matrix_index(r, c, lhs_col)] = lhs[matrix_index(r, c, lhs_row)] + rhs[matrix_index(r, c, lhs_row)];
            }
        }
    }

    return out;
}