/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix.h
Purpose:  External CSC Header
*/

#ifndef MATRIX_H
#define MATRIX_H
#include <matrix_const.h>
#include <matrix_types.h>
#include <stdint.h>

// clang-format off

/* Generic Matrix Operations */
void    matrix_d_print       (double* mat, uint8_t row, uint8_t col);
void    matrix_d_set_identity(double* mat, uint8_t row, uint8_t col);
double* matrix_d_multiply    (double* lhs, double* rhs, double* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_row);
double* matrix_d_add         (double* lhs, double* rhs, double* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_row, uint8_t rhs_col);
double* matrix_d_sub         (double* lhs, double* rhs, double* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_row, uint8_t rhs_col);

void   matrix_f_print       (float* mat, uint8_t row, uint8_t col);
void   matrix_f_set_identity(float* mat, uint8_t row, uint8_t col);
float* matrix_f_multiply    (float* lhs, float* rhs, float* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_row);
float* matrix_f_add         (float* lhs, float* rhs, float* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_row, uint8_t rhs_col);
float* matrix_f_sub         (float* lhs, float* rhs, float* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_row, uint8_t rhs_col);

/* Double Precision Specific Sizing Operations */
#define matrix_4x4d_print(mat)              (matrix_d_print             (mat, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4d_set_identity(mat)       (matrix_d_set_identity      (mat, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4d_multiply(lhs, rhs, out) (matrix_d_multiply(lhs, rhs, out, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4d_add(lhs, rhs, out)      (matrix_d_add     (lhs, rhs, out, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4d_sub(lhs, rhs, out)      (matrix_d_sub     (lhs, rhs, out, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4))

#define matrix_3x3d_print(mat)              (matrix_d_print             (mat, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3d_set_identity(mat)       (matrix_d_set_identity      (mat, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3d_multiply(lhs, rhs, out) (matrix_d_multiply(lhs, rhs, out, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3d_add(lhs, rhs, out)      (matrix_d_add     (lhs, rhs, out, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3d_sub(lhs, rhs, out)      (matrix_d_sub     (lhs, rhs, out, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3))

#define matrix_2x2d_print(mat)              (matrix_d_print             (mat, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2d_set_identity(mat)       (matrix_d_set_identity      (mat, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2d_multiply(lhs, rhs, out) (matrix_d_multiply(lhs, rhs, out, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2d_add(lhs, rhs, out)      (matrix_d_add     (lhs, rhs, out, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2d_sub(lhs, rhs, out)      (matrix_d_sub     (lhs, rhs, out, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2))

/* Single Precision Specific Sizing Operations */
#define matrix_4x4f_print(mat)              (matrix_f_print             (mat, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4f_set_identity(mat)       (matrix_f_set_identity      (mat, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4f_multiply(lhs, rhs, out) (matrix_f_multiply(lhs, rhs, out, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4f_add(lhs, rhs, out)      (matrix_f_add     (lhs, rhs, out, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4))
#define matrix_4x4f_sub(lhs, rhs, out)      (matrix_f_sub     (lhs, rhs, out, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4, MATRIX_DIM_4))

#define matrix_3x3f_print(mat)              (matrix_f_print             (mat, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3f_set_identity(mat)       (matrix_f_set_identity      (mat, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3f_multiply(lhs, rhs, out) (matrix_f_multiply(lhs, rhs, out, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3f_add(lhs, rhs, out)      (matrix_f_add     (lhs, rhs, out, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3))
#define matrix_3x3f_sub(lhs, rhs, out)      (matrix_f_sub     (lhs, rhs, out, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3, MATRIX_DIM_3))

#define matrix_2x2f_print(mat)              (matrix_f_print             (mat, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2f_set_identity(mat)       (matrix_f_set_identity      (mat, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2f_multiply(lhs, rhs, out) (matrix_f_multiply(lhs, rhs, out, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2f_add(lhs, rhs, out)      (matrix_f_add     (lhs, rhs, out, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2))
#define matrix_2x2f_sub(lhs, rhs, out)      (matrix_f_sub     (lhs, rhs, out, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2, MATRIX_DIM_2))

#define matrix_index(row, col, col_len) ((row * col_len) + col)

#define matrix_4x4_index(row, col) (matrix_index(row, col, MATRIX_DIM_4))
#define matrix_3x3_index(row, col) (matrix_index(row, col, MATRIX_DIM_3))
#define matrix_2x2_index(row, col) (matrix_index(row, col, MATRIX_DIM_2))

// clang-format on

#endif
