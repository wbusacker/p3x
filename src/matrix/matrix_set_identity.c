/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix.c
Purpose:  CSC data and initialization definitions
*/

#include <matrix_functions.h>

void matrix_d_set_identity(double* mat, uint8_t row, uint8_t col) {
    for (uint8_t r = 0; r < row; r++) {
        for (uint8_t c = 0; c < col; c++) {
            double assignment = 0;
            if (r == c) {
                assignment = 1;
            }
            mat[(r * col) + c] = assignment;
        }
    }
}

void matrix_f_set_identity(float* mat, uint8_t row, uint8_t col) {
    for (uint8_t r = 0; r < row; r++) {
        for (uint8_t c = 0; c < col; c++) {
            float assignment = 0;
            if (r == c) {
                assignment = 1;
            }
            mat[(r * col) + c] = assignment;
        }
    }
}