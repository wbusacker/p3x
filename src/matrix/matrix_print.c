/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix.c
Purpose:  CSC data and initialization definitions
*/

#include <matrix_functions.h>
#include <stdio.h>

void matrix_d_print(double* mat, uint8_t row, uint8_t col) {

    for (uint8_t r = 0; r < row; r++) {
        for (uint8_t c = 0; c < col; c++) {
            printf("%7.3f ", mat[matrix_index(r, c, row)]);
        }
        printf("\n");
    }
}

void matrix_f_print(float* mat, uint8_t row, uint8_t col) {

    for (uint8_t r = 0; r < row; r++) {
        for (uint8_t c = 0; c < col; c++) {
            printf("%7.3f ", mat[matrix_index(r, c, row)]);
        }
        printf("\n");
    }
}