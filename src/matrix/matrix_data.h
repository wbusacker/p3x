/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix_data.h
Purpose:  CSC data declaration
*/

#ifndef MATRIX_DATA_H
#define MATRIX_DATA_H
#include <matrix_const.h>
#include <matrix_types.h>

#endif
