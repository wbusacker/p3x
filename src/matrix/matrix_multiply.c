/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: matrix.c
Purpose:  CSC data and initialization definitions
*/

#include <matrix_functions.h>

double* matrix_d_multiply(double* lhs, double* rhs, double* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_col) {

    for (uint8_t r = 0; r < lhs_row; r++) {
        for (uint8_t c = 0; c < rhs_col; c++) {
            double sum = 0;
            for (uint8_t i = 0; i < lhs_row; i++) {
                sum += lhs[matrix_index(r, i, lhs_row)] * rhs[matrix_index(i, c, lhs_row)];
            }
            out[matrix_index(r, c, lhs_row)] = sum;
        }
    }

    return out;
}

float* matrix_f_multiply(float* lhs, float* rhs, float* out, uint8_t lhs_row, uint8_t lhs_col, uint8_t rhs_col) {

    for (uint8_t r = 0; r < lhs_row; r++) {
        for (uint8_t c = 0; c < rhs_col; c++) {
            float sum = 0;
            for (uint8_t i = 0; i < lhs_row; i++) {
                sum += lhs[matrix_index(r, i, lhs_row)] * rhs[matrix_index(i, c, lhs_row)];
            }
            out[matrix_index(r, c, lhs_row)] = sum;
        }
    }

    return out;
}