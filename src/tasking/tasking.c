/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  P3X
Filename: tasking.c
Purpose:  CSC data and initialization definitions
*/

#include <tasking.h>
#include <tasking_functions.h>

void init_tasking(void) {

    tasking_main_cycle_count = 0;

    return;
}

void teardown_tasking(void) {
    return;
}
