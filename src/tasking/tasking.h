/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  P3X
Filename: tasking.h
Purpose:  External CSC Header
*/

#ifndef TASKING_H
#define TASKING_H
#include <tasking_const.h>
#include <tasking_types.h>

void init_tasking(void);
void teardown_tasking(void);

uint64_t tasking_cycle(struct Tasking_entry* tasks, uint16_t num_tasks);

void tasking_log_cycle_statistics(struct Tasking_entry* tasks, uint16_t num_tasks);

#endif
