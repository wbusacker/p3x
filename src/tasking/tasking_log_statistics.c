/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  P3X
Filename: tasking_template.c
Purpose:  Example Function File
*/

#include <log.h>
#include <tasking_functions.h>

void tasking_log_cycle_statistics(struct Tasking_entry* tasks, uint16_t num_tasks) {

    for (uint64_t task = 0; task < num_tasks; task++) {

        double average_time_ns = tasks[task].total_cycle_execution_time_ns;
        average_time_ns /= tasks[task].cycle_count;

        LOG_ALWAYS("Task ID %d Total Cycles %lld Cycle Times: Min %10lld Max %10lld Ave %14.3f\n",
                   task,
                   tasks[task].cycle_count,
                   tasks[task].minimum_cycle_execution_time_ns,
                   tasks[task].maximum_cycle_execution_time_ns);
    }
}