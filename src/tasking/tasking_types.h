/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  P3X
Filename: tasking_types.h
Purpose:  CSC data types
*/

#ifndef TASKING_TYPES_H
#define TASKING_TYPES_H
#include <stdint.h>
#include <tasking_const.h>

struct Tasking_entry {
    uint64_t (*function)(uint64_t cycle_count, uint64_t mode, void* arg);
    enum Tasking_frequency frequency;
    uint64_t               mode;
    void*                  arguments;
    uint64_t               cycle_count;
    uint64_t               total_cycle_execution_time_ns;
    uint64_t               minimum_cycle_execution_time_ns;
    uint64_t               maximum_cycle_execution_time_ns;
};

#endif
