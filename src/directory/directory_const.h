/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory_const.h
Purpose:  CSC constants
*/

#ifndef DIRECTORY_CONST_H
#define DIRECTORY_CONST_H
#include <platform.h>

#define MAX_LEN_FILESYSTEM_NAME (1024)

#if defined(PLATFORM_IS_NT)
    #define DIRECTORY_SEPERATOR_CHARACTER ('\\')
#elif defined(PLATFORM_IS_POSIX)
    #define DIRECTORY_SEPERATOR_CHARACTER ('/')
#endif

enum Directory_entry_enm { DIRECTORY_FS_TYPE_FILE, DIRECTORY_FS_TYPE_DIRECTORY, DIRECTORY_FS_TYPE_UNKNOWN };

#endif
