/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory.c
Purpose:  CSC data and initialization definitions
*/

#include <directory.h>
#include <directory_data.h>
#include <platform.h>
#include <string.h>

#if defined(PLATFORM_IS_POSIX)
    #include <dirent.h>
    #include <stdlib.h>
#elif defined(PLATFORM_IS_NT)
    #include <windows.h>
#endif

void directory_get_listing(struct Directory_t* directory, const char* name) {

#if defined(PLATFORM_IS_POSIX)

    DIR* directory_access = opendir(name);

    if (directory_access != NULL) {

        strncpy(directory->directory_name, name, MAX_LEN_FILESYSTEM_NAME);

        /* Do one pass to figure out how many elements to initialize */
        directory->num_elements          = 0;
        struct dirent* directory_element = readdir(directory_access);
        while (directory_element != NULL) {

            directory->num_elements++;
            directory_element = readdir(directory_access);
        }

        /* Reset reading from the directory to put us back at the top */
        closedir(directory_access);
        directory_access = opendir(name);

        directory->elements =
          (struct Directory_entry_t*)malloc(directory->num_elements * sizeof(struct Directory_entry_t));

        for (uint32_t i = 0; i < directory->num_elements; i++) {

            directory_element = readdir(directory_access);

            strncpy(directory->elements[i].name, directory_element->d_name, MAX_LEN_FILESYSTEM_NAME);
            directory->elements[i].name_len = strlen(directory->elements[i].name);

            switch (directory_element->d_type) {
                case DT_REG:
                    directory->elements[i].type = DIRECTORY_FS_TYPE_FILE;
                    break;
                case DT_DIR:
                    directory->elements[i].type = DIRECTORY_FS_TYPE_DIRECTORY;
                    break;
                default:
                    directory->elements[i].type = DIRECTORY_FS_TYPE_UNKNOWN;
            }
        }

        closedir(directory_access);
    } else {
        memset(directory, 0, sizeof(struct Directory_t));
    }

#elif defined(PLATFORM_IS_NT)

    char search_path[MAX_LEN_FILESYSTEM_NAME];

    strncpy(search_path, name, MAX_LEN_FILESYSTEM_NAME);
    strcat(search_path, "\\*");

    WIN32_FIND_DATA directory_element;
    HANDLE          directory_access = FindFirstFileA(search_path, &directory_element);

    if (directory_access != INVALID_HANDLE_VALUE) {

        strncpy(directory->directory_name, name, MAX_LEN_FILESYSTEM_NAME);

        /* If we got this far we know there is at least one file in the directory */
        directory->num_elements = 1;

        while (FindNextFile(directory_access, &directory_element) != 0) {
            directory->num_elements++;
        }

        /* Reset the directory search and buffer for actually scanning */
        FindClose(directory_access);
        directory_access = FindFirstFileA(search_path, &directory_element);

        directory->elements =
          (struct Directory_entry_t*)malloc(directory->num_elements * sizeof(struct Directory_entry_t));

        for (uint32_t i = 0; i < directory->num_elements; i++) {

            strncpy(directory->elements[i].name, directory_element.cFileName, MAX_LEN_FILESYSTEM_NAME);
            directory->elements[i].name_len = strlen(directory->elements[i].name);

            if (directory_element.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                directory->elements[i].type = DIRECTORY_FS_TYPE_DIRECTORY;
            } else {
                directory->elements[i].type = DIRECTORY_FS_TYPE_FILE;
            }

            FindNextFile(directory_access, &directory_element);
        }
        FindClose(directory_access);
    }
#endif
}