/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory.c
Purpose:  CSC data and initialization definitions
*/

#include <directory.h>
#include <directory_data.h>

uint8_t init_directory(void) {
    return 0;
}

uint8_t teardown_directory(void) {
    return 0;
}
