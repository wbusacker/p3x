/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory_data.h
Purpose:  CSC data declaration
*/

#ifndef DIRECTORY_DATA_H
#define DIRECTORY_DATA_H
#include <directory_const.h>
#include <directory_types.h>

#endif
