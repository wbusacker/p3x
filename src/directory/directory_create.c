/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory.c
Purpose:  CSC data and initialization definitions
*/

#include <directory.h>
#include <directory_data.h>
#include <platform.h>

#if defined(PLATFORM_IS_POSIX)
    #include <errno.h>
    #include <sys/stat.h>
#elif defined(PLATFORM_IS_NT)
    #include <windows.h>
#endif

#include <stdio.h>
#include <string.h>

bool directory_create(const char* path) {

    uint32_t num_characters = 0;

    /* Attempt to create the directories all the way down */

    char full_path[MAX_LEN_FILESYSTEM_NAME];
    memset(full_path, 0, MAX_LEN_FILESYSTEM_NAME);

    for (uint16_t i = 0; i < MAX_LEN_FILESYSTEM_NAME; i++) {

        full_path[i] = path[i];
        if (path[i] != 0) {
            /* Copy characters over one by one until we hit a file seperator */
            if (full_path[i] == DIRECTORY_SEPERATOR_CHARACTER) {
#if defined(PLATFORM_IS_NT)
                if (CreateDirectoryA((LPCSTR)full_path, NULL) == FALSE) {
                    break;
                }
#elif defined(PLATFORM_IS_POSIX)
                if (mkdir(full_path, 0755) != 0) {
                    if (errno != EEXIST) {
                        break;
                    }
                }
#endif
            }

        } else {
/* If we reach the end of the string, attempt to create the last remaining directory */
#if defined(PLATFORM_IS_NT)
            CreateDirectoryA((LPCSTR)full_path, NULL);
#elif defined(PLATFORM_IS_POSIX)
            mkdir(full_path, 0755);
#endif
            /* Abort early if we reach the path null terminator */
            break;
        }

        num_characters++;
    }

    return num_characters == strlen(path);
}