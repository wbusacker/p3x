/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory_functions.h
Purpose:  CSC local function declarations
*/

#ifndef DIRECTORY_FUNCTIONS_H
#define DIRECTORY_FUNCTIONS_H
#include <directory.h>
#include <directory_data.h>

#endif
