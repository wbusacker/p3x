/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory.c
Purpose:  CSC data and initialization definitions
*/

#include <directory.h>
#include <directory_data.h>
#include <platform.h>

#if defined(PLATFORM_IS_POSIX)
    #include <string.h>
    #include <unistd.h>
#elif defined(PLATFORM_IS_NT)
    #include <windows.h>
#endif

uint32_t directory_get_current_name(char* name) {

    uint32_t num_characters = 0;

#if defined(PLATFORM_IS_POSIX)
    getcwd(name, MAX_LEN_FILESYSTEM_NAME);
    num_characters = strlen(name);
#elif defined(PLATFORM_IS_NT)
    num_characters = GetCurrentDirectory(MAX_LEN_FILESYSTEM_NAME, name);
#endif

    return num_characters;
}