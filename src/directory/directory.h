/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory.h
Purpose:  External CSC Header
*/

#ifndef DIRECTORY_H
#define DIRECTORY_H
#include <directory_const.h>
#include <directory_types.h>
#include <stdint.h>

uint8_t init_directory(void);
uint8_t teardown_directory(void);

uint32_t directory_get_current_name(char* name);

void directory_get_listing(struct Directory_t* directory, const char* name);
bool directory_create(const char* path);

#endif
