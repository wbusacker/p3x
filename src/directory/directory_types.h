/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: directory_types.h
Purpose:  CSC data types
*/

#ifndef DIRECTORY_TYPES_H
#define DIRECTORY_TYPES_H
#include <directory_const.h>
#include <stdbool.h>
#include <stdint.h>

struct Directory_entry_t {
    char                     name[MAX_LEN_FILESYSTEM_NAME];
    uint32_t                 name_len;
    enum Directory_entry_enm type;
};

struct Directory_t {
    char                      directory_name[MAX_LEN_FILESYSTEM_NAME];
    uint32_t                  directory_name_len;
    struct Directory_entry_t* elements;
    uint32_t                  num_elements;
};

#endif
