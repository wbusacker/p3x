/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error_types.h
Purpose:  CSC data types
*/

#ifndef P3X_ERROR_TYPES_H
#define P3X_ERROR_TYPES_H
#include <p3x_error_const.h>

#endif
