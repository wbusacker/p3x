/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error_functions.h
Purpose:  CSC local function declarations
*/

#ifndef P3X_ERROR_FUNCTIONS_H
#define P3X_ERROR_FUNCTIONS_H
#include <p3x_error.h>
#include <p3x_error_data.h>

#endif
