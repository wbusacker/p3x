/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error_const.h
Purpose:  CSC constants
*/

#ifndef P3X_ERROR_CONST_H
#define P3X_ERROR_CONST_H

#define P3X_ERROR_STRING_BUFFER_LEN (512)

#endif
