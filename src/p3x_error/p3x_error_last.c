/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error.c
Purpose:  CSC data and initialization definitions
*/
#include <p3x_error.h>
#include <platform.h>

#if defined(PLATFORM_IS_NT)
    #include <windows.h>
#elif defined(PLATFORM_IS_POSIX)
    #include <errno.h>
#endif

uint64_t p3x_error_last(void) {

    uint64_t error_code = 0;

#if defined(PLATFORM_IS_NT)
    error_code = GetLastError();
#elif defined(PLATFORM_IS_POSIX)
    error_code = errno;
#endif

    return error_code;
}