/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error.c
Purpose:  CSC data and initialization definitions
*/

#include <p3x_error.h>
#include <p3x_error_data.h>

uint8_t init_p3x_error(void) {
}

uint8_t teardown_p3x_error(void) {
}
