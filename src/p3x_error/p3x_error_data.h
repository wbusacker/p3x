/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error_data.h
Purpose:  CSC data declaration
*/

#ifndef P3X_ERROR_DATA_H
#define P3X_ERROR_DATA_H
#include <p3x_error_const.h>
#include <p3x_error_types.h>

#endif
