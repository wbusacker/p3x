/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error.c
Purpose:  CSC data and initialization definitions
*/
#include <p3x_error.h>
#include <platform.h>
#include <string.h>

#if defined(PLATFORM_IS_NT)
    #include <windows.h>
#elif defined(PLATFORM_IS_POSIX)
    #include <errno.h>
#endif

const uint8_t* p3x_error_last_string(void) {

    static uint8_t p3x_error_last_string_buffer[P3X_ERROR_STRING_BUFFER_LEN];
    memset(p3x_error_last_string_buffer, 0, P3X_ERROR_STRING_BUFFER_LEN);

    uint64_t error_code = p3x_error_last();

#if defined(PLATFORM_IS_NT)
    LPVOID message_buffer;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                  NULL,
                  error_code,
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                  (LPTSTR)&message_buffer,
                  0,
                  NULL);
    strcpy(p3x_error_last_string_buffer, message_buffer);
    LocalFree(message_buffer);
#elif defined(PLATFORM_IS_POSIX)
    strcpy(p3x_error_last_string_buffer, strerror(error_code));
#endif

    return p3x_error_last_string_buffer;
}