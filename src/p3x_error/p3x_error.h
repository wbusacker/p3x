/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: p3x_error.h
Purpose:  External CSC Header
*/

#ifndef P3X_ERROR_H
#define P3X_ERROR_H
#include <p3x_error_const.h>
#include <p3x_error_types.h>
#include <stdint.h>

uint8_t init_p3x_error(void);
uint8_t teardown_p3x_error(void);

const uint8_t* p3x_error_last_string(void);
uint64_t       p3x_error_last(void);

#endif
