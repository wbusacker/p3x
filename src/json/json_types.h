/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_types.h
Purpose:  CSC data types
*/

#ifndef JSON_TYPES_H
#define JSON_TYPES_H

#include <json_const.h>
#include <stdbool.h>

/* Because these are all circularly referential, pre-declare these structs */
struct JSON_Value;
struct JSON_Keypair;

struct JSON_Object {
    struct JSON_Keypair* elements;
    uint32_t             num_elements;
};

struct JSON_String {
    uint8_t* string;
    uint32_t string_length;
};

struct JSON_Array {
    struct JSON_Value* values;
    uint32_t           num_values;
};

struct JSON_Value {
    struct JSON_String       string;
    double                   number;
    struct JSON_Object       object;
    struct JSON_Array        array;
    bool                     boolean;
    bool                     is_null;
    enum JSON_Data_types_enm datatype;
};

struct JSON_Keypair {
    struct JSON_String key;
    struct JSON_Value  value;
};

#endif
