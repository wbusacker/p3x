/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_const.h
Purpose:  CSC constants
*/

#ifndef JSON_CONST_H
#define JSON_CONST_H

#include <stdint.h>

#define DEFAULT_LIST_SIZE                     (1024 * 1024)
#define NUM_JSON_WHITESPACE_CHARACTERS        (4)
#define NUM_JSON_ESCAPED_CHARACTERS           (8)
#define JSON_BAD_IMPORT                       (-1)
#define JSON_ASCII_NUMBER_START               (0x30)
#define JSON_NUMBER_BASE                      (10)
#define JSON_TRUE_STRING                      ("true")
#define JSON_FALSE_STRING                     ("false")
#define JSON_NULL_STRING                      ("null")
#define JSON_NUMBER_FLOAT_EXPORT_FORMAT       "%23.16e"
#define JSON_NUMBER_INTEGER_EXPORT_FORMAT     "%22ld"
#define JSON_NUMBER_EXPORT_DESTINATION_LENGTH (23)

#define JSON_INVALID_INDEX (0xFFFFFFFF)

extern uint8_t JSON_WHITESPACE_CHARACTERS[NUM_JSON_WHITESPACE_CHARACTERS];
extern uint8_t JSON_ESCAPED_CHARACTERS[NUM_JSON_ESCAPED_CHARACTERS];
extern uint8_t JSON_ESCAPED_CODES[NUM_JSON_ESCAPED_CHARACTERS];

enum JSON_Data_types_enm { JSON_STRING, JSON_NUMBER, JSON_OBJECT, JSON_ARRAY, JSON_BOOL, JSON_NULL, JSON_ERROR };

#endif
