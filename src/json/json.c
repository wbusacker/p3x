/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json.c
Purpose:  CSC data and initialization definitions
*/

#include <json.h>
#include <json_data.h>

uint8_t init_json(void) {
    return 0;
}

uint8_t teardown_json(void) {
    return 0;
}
