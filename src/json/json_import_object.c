/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

int32_t json_import_object(uint8_t* text, struct JSON_Object* object) {

    int32_t read_bytes = JSON_BAD_IMPORT;

    /* First, build a temporary buffer to store keypairs in as we import them */
    struct JSON_Keypair* seen_keypairs = (struct JSON_Keypair*)malloc(DEFAULT_LIST_SIZE * sizeof(struct JSON_Keypair));
    memset(seen_keypairs, 0, DEFAULT_LIST_SIZE * sizeof(struct JSON_Keypair));

    uint8_t* text_scan   = text;
    object->num_elements = 0;

    /* Step 1: Ensure that the first character we read is a curly open */
    if (*text_scan == '{') {
        text_scan++;

        /* Step 2: We'll always start with some whitespace */
        SKIP_WHITESPACE(text_scan);

        /* Step 3: Either we have a string or we have a string to collect
            If we close the Object, safely fall out. Specifically don't increment
            the text scan point on this check
        */
        bool object_closed = false;

        if (*text_scan == '}') {
            object_closed = true;
        }

        while (! object_closed) {

            /* Step 4: Collect the string for the keypair name */
            read_bytes = json_import_string(text_scan, &(seen_keypairs[object->num_elements].key));

            if (read_bytes == JSON_BAD_IMPORT) {
                break;
            }
            text_scan += read_bytes;

            /* Step 5: Ensure that we get a colon next */
            SKIP_WHITESPACE(text_scan);

            if (*text_scan != ':') {
                json_read_failure_index = text_scan;
                read_bytes              = JSON_BAD_IMPORT;
                break;
            }
            text_scan++;

            /* Step 6: Collect the value for the keypair */
            read_bytes = json_import_value(text_scan, &(seen_keypairs[object->num_elements].value));

            if (read_bytes == JSON_BAD_IMPORT) {
                break;
            }
            text_scan += read_bytes;

            /* Step 7: Figure out if the object is closed or not */
            if (*text_scan == '}') {
                object_closed = true;
            } else if (*text_scan != ',') {
                /* We shouldn't have hit anything else here */
                json_read_failure_index = text_scan;
                read_bytes              = JSON_BAD_IMPORT;
                break;
            }
            text_scan++;

            /* Step 8: If looping around, clear up whitespace for the key string */
            SKIP_WHITESPACE(text_scan);
            object->num_elements++;
        }
    } else {
        json_read_failure_index = text_scan;
        read_bytes              = JSON_BAD_IMPORT;
    }

    /* If we had a successful object import, allocate the real memory space and copy everything over */
    if (read_bytes != JSON_BAD_IMPORT) {

        object->elements = (struct JSON_Keypair*)malloc(object->num_elements * sizeof(struct JSON_Keypair));

        for (uint32_t i = 0; i < object->num_elements; i++) {
            object->elements[i] = seen_keypairs[i];
        }

        /* Recalculate the real and actual number of read bytes */
        read_bytes = (text_scan - text);
    }

    free(seen_keypairs);

    return read_bytes;
}