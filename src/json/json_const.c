/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_const.c
Purpose:  CSC constants
*/

#include <json_const.h>

uint8_t JSON_WHITESPACE_CHARACTERS[NUM_JSON_WHITESPACE_CHARACTERS] = {' ', '\n', '\r', '\t'};
uint8_t JSON_ESCAPED_CHARACTERS[NUM_JSON_ESCAPED_CHARACTERS]       = {'"', '\\', 'b', 'f', 'n', 'r', 't'};
uint8_t JSON_ESCAPED_CODES[NUM_JSON_ESCAPED_CHARACTERS]            = {'"', '\\', '\b', '\f', '\n', '\r', '\t'};
