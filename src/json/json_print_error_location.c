/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_text_object.c
Purpose:  Imports a JSON object from text buffer
*/

#include <json_functions.h>
#include <log.h>
#include <stdio.h>
#include <stdlib.h>

void json_print_error_location(uint8_t* text) {

    if (json_read_failure_index == 0) {
        LOG("P3X JSON INTERNAL ERROR\n");
    } else {
        uint64_t line_number   = 1;
        uint64_t column_number = 0;

        uint8_t* text_scan = text;

        while (text_scan != json_read_failure_index) {
            if (*text_scan == '\n') {
                line_number++;
                column_number = 0;
            } else {
                column_number++;
            }

            text_scan++;
        }

        LOG("Warning, invalid JSON at Line %ld Column %ld\n", line_number, column_number);
    }
}