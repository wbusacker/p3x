/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

uint32_t json_get_object_keypair_index(struct JSON_Object* object, char* key, enum JSON_Data_types_enm type) {

    uint32_t keypair_index = JSON_INVALID_INDEX;

    for (uint32_t i = 0; i < object->num_elements; i++) {

        if ((strncmp(key, (char*)object->elements[i].key.string, object->elements[i].key.string_length) == 0) &&
            (object->elements[i].value.datatype == type)) {
            keypair_index = i;
            break;
        }
    }

    return keypair_index;
}