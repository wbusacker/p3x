/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

void increment_element_buffer_len(struct JSON_Object* object) {

    uint32_t new_element_count = object->num_elements + 1;

    struct JSON_Keypair* new_keypair_list =
      (struct JSON_Keypair*)malloc(sizeof(struct JSON_Keypair) * new_element_count);

    for (uint32_t i = 0; i < object->num_elements; i++) {
        new_keypair_list[i] = object->elements[i];
    }

    free(object->elements);
    object->elements     = new_keypair_list;
    object->num_elements = new_element_count;
}

void json_add_object_string(struct JSON_Object* object, char* key, struct JSON_String* string) {

    /* Make sure we don't already have this key pair */
    uint32_t index = json_get_object_keypair_index(object, key, JSON_STRING);

    if (index == JSON_INVALID_INDEX) {

        increment_element_buffer_len(object);

        struct JSON_String* new_key                    = json_build_string(key);
        object->elements[object->num_elements - 1].key = *new_key;
        free(new_key);

        object->elements[object->num_elements - 1].value.datatype = JSON_STRING;
        object->elements[object->num_elements - 1].value.string   = *string;
    }
}

void json_add_object_number(struct JSON_Object* object, char* key, double* number) {
    uint32_t index = json_get_object_keypair_index(object, key, JSON_NUMBER);

    if (index == JSON_INVALID_INDEX) {

        increment_element_buffer_len(object);

        struct JSON_String* new_key                    = json_build_string(key);
        object->elements[object->num_elements - 1].key = *new_key;
        free(new_key);

        object->elements[object->num_elements - 1].value.datatype = JSON_NUMBER;
        object->elements[object->num_elements - 1].value.number   = *number;
    }
}

void json_add_object_object(struct JSON_Object* object, char* key, struct JSON_Object* new_object) {

    uint32_t index = json_get_object_keypair_index(object, key, JSON_OBJECT);

    if (index == JSON_INVALID_INDEX) {

        increment_element_buffer_len(object);

        struct JSON_String* new_key                    = json_build_string(key);
        object->elements[object->num_elements - 1].key = *new_key;
        free(new_key);

        object->elements[object->num_elements - 1].value.datatype = JSON_OBJECT;
        object->elements[object->num_elements - 1].value.object   = *new_object;
    }
}

void json_add_object_array(struct JSON_Object* object, char* key, struct JSON_Array* array) {

    uint32_t index = json_get_object_keypair_index(object, key, JSON_ARRAY);

    if (index == JSON_INVALID_INDEX) {

        increment_element_buffer_len(object);

        struct JSON_String* new_key                    = json_build_string(key);
        object->elements[object->num_elements - 1].key = *new_key;
        free(new_key);

        object->elements[object->num_elements - 1].value.datatype = JSON_ARRAY;
        object->elements[object->num_elements - 1].value.array    = *array;
    }
}

void json_add_object_boolean(struct JSON_Object* object, char* key, bool* boolean) {

    uint32_t index = json_get_object_keypair_index(object, key, JSON_BOOL);

    if (index == JSON_INVALID_INDEX) {

        increment_element_buffer_len(object);

        struct JSON_String* new_key                    = json_build_string(key);
        object->elements[object->num_elements - 1].key = *new_key;
        free(new_key);

        object->elements[object->num_elements - 1].value.datatype = JSON_BOOL;
        object->elements[object->num_elements - 1].value.boolean  = *boolean;
    }
}

void json_add_object_is_null(struct JSON_Object* object, char* key, bool* is_null) {

    uint32_t index = json_get_object_keypair_index(object, key, JSON_NULL);

    if (index == JSON_INVALID_INDEX) {

        increment_element_buffer_len(object);

        struct JSON_String* new_key                    = json_build_string(key);
        object->elements[object->num_elements - 1].key = *new_key;
        free(new_key);

        object->elements[object->num_elements - 1].value.datatype = JSON_NULL;
        object->elements[object->num_elements - 1].value.is_null  = *is_null;
    }
}
