/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_text_object.c
Purpose:  Imports a JSON object from text buffer
*/

#include <json_functions.h>
#include <stdio.h>
#include <stdlib.h>

struct JSON_Object* json_import_text_object(uint8_t* text, uint64_t length) {

    json_read_failure_index = NULL;

    struct JSON_Object* object = NULL;

    if (json_check_recmatch(text, length)) {
        object             = (struct JSON_Object*)malloc(sizeof(struct JSON_Object));
        uint8_t* text_scan = text;

        SKIP_WHITESPACE(text_scan);

        int32_t imported_bytes = json_import_object(text_scan, object);

        if (imported_bytes == JSON_BAD_IMPORT) {
            json_print_error_location(text);
            free(object);
            object = NULL;
        }
    }

    return object;
}