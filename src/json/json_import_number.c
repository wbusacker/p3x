/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_number.c
Purpose:  Imports a double from text
*/

#include <json_functions.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int32_t collect_digits(uint8_t* text, double* number) {

    int32_t read_digits = 0;

    *number = 0;

    bool keep_importing = true;
    while (keep_importing) {

        /* Get the digit and check to see if its within the range
            of a base 10 digit
        */
        uint8_t digit = (*text) - JSON_ASCII_NUMBER_START;
        if (digit < JSON_NUMBER_BASE) {
            *number *= JSON_NUMBER_BASE;
            *number += digit;
            text++;
            read_digits++;
        } else {
            keep_importing = false;
        }
    }

    return read_digits;
}

/* In the interest of clarity, this function utilizes early return
    to make the import state machine easier to follow through
    on its error conditions
*/
int32_t json_import_number(uint8_t* text, double* number) {

    int32_t  read_bytes = JSON_BAD_IMPORT;
    uint8_t* text_scan  = text;

    double integer           = 0;
    double fraction          = 0;
    double exponent          = 0;
    bool   negative_mantissa = false;
    bool   negative_exponent = false;
    bool   has_decimal       = false;
    bool   has_fraction      = false;
    bool   has_exponent      = false;

    /* Step 1: Check if the number is Negative
        We do no fail out if this check fails
    */

    if (*text_scan == '-') {
        negative_mantissa = true;
        text_scan++;
    }

    /* Step 2: If the next character is 0, we must advance
        to the next stage. Else collect the digits
    */

    if (*text_scan != '0') {
        has_decimal = true;
        read_bytes  = collect_digits(text_scan, &integer);

        if (read_bytes == 0) {
            /* For this to be a valid import, we must have collected at
                least one single digit
            */
            json_read_failure_index = text_scan;
            return JSON_BAD_IMPORT;
        } else {
            text_scan += read_bytes;
        }
    } else {
        text_scan++;
        read_bytes = 1;
    }

    /* Step 3: If we have a fractional part, collect it */
    if (*text_scan == '.') {
        has_fraction = true;
        text_scan++;
        read_bytes = collect_digits(text_scan, &fraction);
        if (read_bytes == 0) {
            /* For this to be a valid import, we must have collected at
                least one single digit
            */
            json_read_failure_index = text_scan;
            return JSON_BAD_IMPORT;
        } else {
            text_scan += read_bytes;
            fraction /= pow(JSON_NUMBER_BASE, read_bytes);
        }
    }

    /* Step 4: If we have an exponent, collect it */
    if ((*text_scan == 'e') || (*text_scan == 'E')) {
        has_exponent = true;
        text_scan++;

        /* Here we have an extra special case. We can
            accept either a -, + or neither preceeding
            the digits of the exponent
        */
        if (*text_scan == '-') {
            negative_exponent = true;
            text_scan++;
        } else if (*text_scan == '+') {
            text_scan++;
        }

        read_bytes = collect_digits(text_scan, &exponent);
        if (read_bytes == 0) {
            /* For this to be a valid import, we must have collected at
                least one single digit
            */
            json_read_failure_index = text_scan;
            return JSON_BAD_IMPORT;
        } else {
            text_scan += read_bytes;
        }
    }

    if (negative_exponent) {
        exponent *= -1;
    }

    if (has_decimal || has_fraction || has_exponent) {

        *number = (integer + fraction) * pow(JSON_NUMBER_BASE, exponent);

        if (negative_mantissa) {
            *number *= -1;
        }

    } else {
        /* Something else went wrong during the import. We can't not have one of them */
        json_read_failure_index = text_scan;
        return JSON_BAD_IMPORT;
    }

    read_bytes = (text_scan - text);

    return read_bytes;
}