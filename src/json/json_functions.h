/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_functions.h
Purpose:  CSC local function declarations
*/

#ifndef JSON_FUNCTIONS_H
#define JSON_FUNCTIONS_H
#include <json.h>
#include <json_data.h>
#include <long_buffer.h>

int32_t json_import_object(uint8_t* text, struct JSON_Object* object);
int32_t json_import_array(uint8_t* text, struct JSON_Array* array);
int32_t json_import_value(uint8_t* text, struct JSON_Value* value);
int32_t json_import_string(uint8_t* text, struct JSON_String* string);
int32_t json_import_number(uint8_t* text, double* number);

void json_export_object(struct Long_buffer* buffer, struct JSON_Object* object);
void json_export_array(struct Long_buffer* buffer, struct JSON_Array* array);
void json_export_value(struct Long_buffer* buffer, struct JSON_Value* value);
void json_export_string(struct Long_buffer* buffer, struct JSON_String* string);

void json_delete_string(struct JSON_String* string);

bool json_check_recmatch(uint8_t* text, uint64_t length);
void json_print_error_location(uint8_t* text);

bool json_test_is_whitespace(uint8_t character);

uint32_t json_get_object_keypair_index(struct JSON_Object* object, char* key, enum JSON_Data_types_enm type);

#ifdef _MSC_VER

    #define SKIP_WHITESPACE(x)                                                                                         \
        {                                                                                                              \
            while (json_test_is_whitespace(*x)) {                                                                      \
                x++;                                                                                                   \
            }                                                                                                          \
        }

#else

    #define SKIP_WHITESPACE(x)                                                                                         \
        ({                                                                                                             \
            while (json_test_is_whitespace(*x)) {                                                                      \
                x++;                                                                                                   \
            }                                                                                                          \
        })
#endif

#endif