/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>

void json_export_array(struct Long_buffer* buffer, struct JSON_Array* array) {

    long_buffer_append(buffer, (uint8_t*)"[", 1);

    for (uint64_t i = 0; i < array->num_values; i++) {
        if (i != 0) {
            long_buffer_append(buffer, (uint8_t*)",", 1);
        }
        json_export_value(buffer, &(array->values[i]));
    }

    long_buffer_append(buffer, (uint8_t*)"]", 1);

    return;
}
