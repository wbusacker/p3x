/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_string.c
Purpose:  Imports a JSON string from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

int32_t json_import_string(uint8_t* text, struct JSON_String* string) {

    int32_t read_bytes = JSON_BAD_IMPORT;

    string->string        = NULL;
    string->string_length = 0;

    uint8_t* text_scan = text;

    /* Step 1: Ensure that the first character we read is a quotation mark */
    if (*text_scan == '"') {

        text_scan++;
        uint8_t* string_start = text_scan;

        /*Step 2: Collect characters until we see an un-escaped quote*/
        bool keep_reading = true;
        while (keep_reading) {

            /* Step 3: Check if its an escape character */
            if (*text_scan == '\\') {

                /* Validate what we're escaping */
                bool valid_escape = false;
                text_scan++;
                for (uint8_t i = 0; i < NUM_JSON_ESCAPED_CHARACTERS; i++) {
                    if (*text_scan == JSON_ESCAPED_CHARACTERS[i]) {
                        valid_escape = true;
                    }
                }

                if (! valid_escape) {
                    keep_reading            = false;
                    json_read_failure_index = text_scan;
                    string->string_length   = 0;
                } else {
                    text_scan++;
                    string->string_length++;
                }
            } else if (*text_scan == '"') {
                /* Clean string exit */
                keep_reading   = false;
                string->string = (uint8_t*)malloc(string->string_length * sizeof(uint8_t) + 1);
                memcpy(string->string, string_start, string->string_length);
                string->string[string->string_length] = '\0';
                read_bytes                            = (text_scan - text) + 1;
            } else {

                text_scan++;
                string->string_length++;
            }
        }
    } else {
        read_bytes              = JSON_BAD_IMPORT;
        json_read_failure_index = text_scan;
    }

    return read_bytes;
}