/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_array.c
Purpose:  Imports a JSON array from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

struct JSON_Object* json_build_object(void) {

    struct JSON_Object* new_object = (struct JSON_Object*)malloc(sizeof(struct JSON_Object));

    new_object->elements     = NULL;
    new_object->num_elements = 0;

    return new_object;
}