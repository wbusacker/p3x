/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_array.c
Purpose:  Imports a JSON array from text
*/

#include <json_functions.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void json_export_file_object(struct JSON_Object* object, const char* filename) {

    char* file_contents = json_export_text_object(object);

    FILE* fp = fopen(filename, "w");
    if (fp != NULL) {
        fprintf(fp, "%s", file_contents);
        fclose(fp);
    }

    return;
}