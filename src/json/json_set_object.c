/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

void json_set_object_number(struct JSON_Object* object, char* key, double* number) {
    uint32_t index = json_get_object_keypair_index(object, key, JSON_NUMBER);

    if (index != JSON_INVALID_INDEX) {
        object->elements[index].value.number = *number;
    }
}