/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_value.c
Purpose:  Imports a JSON value from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

int32_t json_import_value(uint8_t* text, struct JSON_Value* value) {

    int32_t  read_bytes = JSON_BAD_IMPORT;
    uint8_t* text_scan  = text;

    /* Read past any more whitespace */
    SKIP_WHITESPACE(text_scan);

    /* Figure out exactly how we want to handle the value */
    if (*text_scan == '{') {
        read_bytes      = json_import_object(text_scan, &(value->object));
        value->datatype = JSON_OBJECT;
    } else if (*text_scan == '[') {
        read_bytes      = json_import_array(text_scan, &(value->array));
        value->datatype = JSON_ARRAY;
    } else if (*text_scan == '"') {
        read_bytes      = json_import_string(text_scan, &(value->string));
        value->datatype = JSON_STRING;
    } else if (strncmp((char*)text_scan, JSON_TRUE_STRING, sizeof(JSON_TRUE_STRING) - 1) == 0) {
        read_bytes      = sizeof(JSON_TRUE_STRING) - 1;
        value->datatype = JSON_BOOL;
        value->boolean  = true;
    } else if (strncmp((char*)text_scan, JSON_FALSE_STRING, sizeof(JSON_FALSE_STRING) - 1) == 0) {
        read_bytes      = sizeof(JSON_FALSE_STRING) - 1;
        value->datatype = JSON_BOOL;
        value->boolean  = false;
    } else if (strncmp((char*)text_scan, JSON_NULL_STRING, sizeof(JSON_NULL_STRING) - 1) == 0) {
        read_bytes      = sizeof(JSON_NULL_STRING) - 1;
        value->datatype = JSON_NULL;
        value->is_null  = true;
    } else {
        read_bytes      = json_import_number(text_scan, &(value->number));
        value->datatype = JSON_NUMBER;
    }

    if (read_bytes != JSON_BAD_IMPORT) {
        text_scan += read_bytes;
        SKIP_WHITESPACE(text_scan);

        read_bytes = text_scan - text;
    } else {
        value->datatype = JSON_ERROR;
    }

    return read_bytes;
}