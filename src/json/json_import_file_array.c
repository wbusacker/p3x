/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_file_array.c
Purpose:  Imports a JSON array from a file
*/

#include <json_functions.h>
#include <stdio.h>
#include <stdlib.h>

struct JSON_Array* json_import_file_array(const char* filename) {

    struct JSON_Array* object = NULL;

    FILE* fp = fopen(filename, "r");

    if (fp != NULL) {
        fseek(fp, 0, SEEK_END);
        uint64_t json_length = ftell(fp);

        /* Figure out how big the file is */
        uint8_t* file_buffer = (uint8_t*)malloc(json_length);
        fseek(fp, 0, SEEK_SET);

        fread(file_buffer, 1, json_length, fp);

        object = json_import_text_array(file_buffer, json_length);

        fclose(fp);
        free(file_buffer);
    }

    return object;
}