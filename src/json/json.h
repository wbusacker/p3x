/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json.h
Purpose:  External CSC Header
*/

#ifndef JSON_H
#define JSON_H
#include <json_const.h>
#include <json_types.h>
#include <stdint.h>

struct JSON_Object* json_build_object(void);
struct JSON_Array*  json_build_array(void);
struct JSON_String* json_build_string(const char* string);

struct JSON_Object* json_import_file_object(const char* filename);
struct JSON_Array*  json_import_file_array(const char* filename);

struct JSON_Object* json_import_text_object(uint8_t* text, uint64_t length);
struct JSON_Array*  json_import_text_array(uint8_t* text, uint64_t length);

char* json_export_text_object(struct JSON_Object* object);
void  json_export_file_object(struct JSON_Object* object, const char* filename);

struct JSON_String* json_access_object_string(struct JSON_Object* object, char* key);
double              json_access_object_number(struct JSON_Object* object, char* key);
struct JSON_Object* json_access_object_object(struct JSON_Object* object, char* key);
struct JSON_Array*  json_access_object_array(struct JSON_Object* object, char* key);
bool                json_access_object_boolean(struct JSON_Object* object, char* key);
bool                json_access_object_is_null(struct JSON_Object* object, char* key);

struct JSON_String* json_access_array_string(struct JSON_Array* array, uint64_t index);
double              json_access_array_number(struct JSON_Array* array, uint64_t index);
struct JSON_Object* json_access_array_object(struct JSON_Array* array, uint64_t index);
struct JSON_Array*  json_access_array_array(struct JSON_Array* array, uint64_t index);
bool                json_access_array_boolean(struct JSON_Array* array, uint64_t index);
bool                json_access_array_is_null(struct JSON_Array* array, uint64_t index);

void json_add_object_string(struct JSON_Object* object, char* key, struct JSON_String* string);
void json_add_object_number(struct JSON_Object* object, char* key, double* number);
void json_add_object_object(struct JSON_Object* object, char* key, struct JSON_Object* new_object);
void json_add_object_array(struct JSON_Object* object, char* key, struct JSON_Array* array);
void json_add_object_boolean(struct JSON_Object* object, char* key, bool* boolean);
void json_add_object_is_null(struct JSON_Object* object, char* key, bool* is_null);

void json_set_object_number(struct JSON_Object* object, char* key, double* number);

void json_delete_object(struct JSON_Object* object);
void json_delete_array(struct JSON_Array* array);

uint8_t init_json(void);
uint8_t teardown_json(void);

#endif
