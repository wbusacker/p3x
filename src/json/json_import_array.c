/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_array.c
Purpose:  Imports a JSON array from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

int32_t json_import_array(uint8_t* text, struct JSON_Array* array) {

    int32_t read_bytes = JSON_BAD_IMPORT;

    /* First, build a temporary buffer to store keypairs in as we import them */
    struct JSON_Value* seen_values = (struct JSON_Value*)malloc(DEFAULT_LIST_SIZE * sizeof(struct JSON_Value));
    memset(seen_values, 0, DEFAULT_LIST_SIZE * sizeof(struct JSON_Value));

    uint8_t* text_scan = text;
    array->num_values  = 0;

    /* Step 1: Ensure that the first character we read is a curly open */
    if (*text_scan == '[') {
        text_scan++;

        /* Step 2: We'll always start with some whitespace */
        SKIP_WHITESPACE(text_scan);

        /* Step 3: Either we have a string or we have a string to collect
            If we close the Object, safely fall out.
        */
        bool object_closed = false;

        if (*text_scan == ']') {
            read_bytes += 2;
            object_closed = true;
        }

        while (! object_closed) {

            /* Step 4: Collect the value */
            read_bytes = json_import_value(text_scan, &(seen_values[array->num_values]));

            if (read_bytes == JSON_BAD_IMPORT) {
                break;
            }
            text_scan += read_bytes;

            /* Step 5: Figure out if the object is closed or not */
            if (*text_scan == ']') {
                object_closed = true;
            } else if (*text_scan != ',') {
                /* We shouldn't have hit anything else here */
                json_read_failure_index = text_scan;
                read_bytes              = JSON_BAD_IMPORT;
                break;
            }
            text_scan++;

            array->num_values++;
        }
    } else {
        json_read_failure_index = text_scan;
        read_bytes              = JSON_BAD_IMPORT;
    }

    /* If we had a successful object import, allocate the real memory space and copy everything over */
    if (read_bytes != JSON_BAD_IMPORT) {

        array->values = (struct JSON_Value*)malloc(array->num_values * sizeof(struct JSON_Value));

        for (uint32_t i = 0; i < array->num_values; i++) {
            array->values[i] = seen_values[i];
        }

        /* Recalculate the real and actual number of read bytes */
        read_bytes = (text_scan - text);
    }

    free(seen_values);

    return read_bytes;
}