/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_array.c
Purpose:  Imports a JSON array from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

char* json_export_text_object(struct JSON_Object* object) {

    struct Long_buffer* buffer = long_buffer_build();

    json_export_object(buffer, object);

    uint64_t text_length = buffer->total_data_len + 2;

    char* text = (char*)malloc(text_length);

    memset(text, 0, text_length);

    long_buffer_copy_out(buffer, text, text_length);

    long_buffer_delete(buffer);

    return text;
}