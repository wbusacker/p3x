/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_test_is_whitespace.c
Purpose:  Checks if a given character is valid JSON whitespace
*/

#include <json_functions.h>

bool json_test_is_whitespace(uint8_t character) {

    bool answer = false;

    for (uint8_t i = 0; i < NUM_JSON_WHITESPACE_CHARACTERS; i++) {
        if (character == JSON_WHITESPACE_CHARACTERS[i]) {
            answer = true;
        }
    }

    return answer;
}