/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_array.c
Purpose:  Imports a JSON array from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

struct JSON_String* json_build_string(const char* string) {

    struct JSON_String* new_string = (struct JSON_String*)malloc(sizeof(struct JSON_String));

    new_string->string_length = strlen(string);
    new_string->string        = (uint8_t*)malloc(new_string->string_length + 1);
    strcpy((char*)new_string->string, string);

    return new_string;
}