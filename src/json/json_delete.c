/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_delete.c
Purpose:  Frees memory held by a JSON object
*/

#include <json_functions.h>
#include <stdlib.h>

void json_delete_object(struct JSON_Object* object) {

    for (uint32_t i = 0; i < object->num_elements; i++) {

        switch (object->elements[i].value.datatype) {
            case JSON_STRING:
                json_delete_string(&(object->elements[i].value.string));
                break;
            case JSON_OBJECT:
                json_delete_object(&(object->elements[i].value.object));
                break;
            case JSON_ARRAY:
                json_delete_array(&(object->elements[i].value.array));
                break;
            default:
                break;
        }

        json_delete_string(&(object->elements[i].key));
    }

    free(object->elements);
}

void json_delete_array(struct JSON_Array* array) {
    for (uint32_t i = 0; i < array->num_values; i++) {

        switch (array->values[i].datatype) {

            case JSON_STRING:
                json_delete_string(&(array->values[i].string));
                break;
            case JSON_OBJECT:
                json_delete_object(&(array->values[i].object));
                break;
            case JSON_ARRAY:
                json_delete_array(&(array->values[i].array));
                break;
            default:
                break;
        }
    }

    free(array->values);
}

void json_delete_string(struct JSON_String* string) {

    free(string->string);
}