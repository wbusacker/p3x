/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_data.h
Purpose:  CSC data declaration
*/

#ifndef JSON_DATA_H
#define JSON_DATA_H
#include <json_const.h>
#include <json_types.h>

extern uint8_t* json_read_failure_index;

#endif
