/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

bool json_check_recmatch(uint8_t* text, uint64_t length) {

    int64_t num_curly  = 0;
    int64_t num_square = 0;
    bool    in_quotes  = false;
    bool    in_escape  = false;

    uint8_t* text_scan = text;

    uint8_t* buffer_end = text + length;

    while (text_scan < buffer_end) {

        if (*text_scan == '\\') {
            text_scan++;
            in_escape = true;

            /* If the next character is the end
               of the string, let the normal
                null termination logic work
            */
            if (*text_scan == '\0') {
                text_scan--;
            } else {
                in_escape = false;
            }

        } else if (*text_scan == '{') {
            num_curly++;
        } else if (*text_scan == '}') {
            num_curly--;
        } else if (*text_scan == '[') {
            num_square++;
        } else if (*text_scan == ']') {
            num_square--;
        } else if (*text_scan == '\"') {
            in_quotes ^= true;
        }

        text_scan++;
    }

    return ((num_curly == 0) && (num_square == 0) && ! in_quotes && ! in_escape);
}