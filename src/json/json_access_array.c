/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

uint32_t json_get_valid_array_index(struct JSON_Array* array, uint64_t index, enum JSON_Data_types_enm type) {

    uint32_t keypair_index = JSON_INVALID_INDEX;

    if ((index < array->num_values) && (array->values[index].datatype == type)) {
        keypair_index = index;
    }

    return keypair_index;
}

struct JSON_String* json_access_array_string(struct JSON_Array* array, uint64_t index) {
    struct JSON_String* data = NULL;
    index                    = json_get_valid_array_index(array, index, JSON_STRING);

    if (index != JSON_INVALID_INDEX) {
        data = &(array->values[index].string);
    }

    return data;
}

double json_access_array_number(struct JSON_Array* array, uint64_t index) {
    double data = 0;
    index       = json_get_valid_array_index(array, index, JSON_NUMBER);

    if (index != JSON_INVALID_INDEX) {
        data = array->values[index].number;
    }

    return data;
}

struct JSON_Object* json_access_array_object(struct JSON_Array* array, uint64_t index) {
    struct JSON_Object* data = NULL;
    index                    = json_get_valid_array_index(array, index, JSON_OBJECT);

    if (index != JSON_INVALID_INDEX) {
        data = &(array->values[index].object);
    }

    return data;
}

struct JSON_Array* json_access_array_array(struct JSON_Array* array, uint64_t index) {
    struct JSON_Array* data = NULL;
    index                   = json_get_valid_array_index(array, index, JSON_ARRAY);

    if (index != JSON_INVALID_INDEX) {
        data = &(array->values[index].array);
    }

    return data;
}

bool json_access_array_boolean(struct JSON_Array* array, uint64_t index) {
    bool data = NULL;
    index     = json_get_valid_array_index(array, index, JSON_BOOL);

    if (index != JSON_INVALID_INDEX) {
        data = array->values[index].boolean;
    }

    return data;
}

bool json_access_array_is_null(struct JSON_Array* array, uint64_t index) {
    bool data = NULL;
    index     = json_get_valid_array_index(array, index, JSON_NULL);

    if (index != JSON_INVALID_INDEX) {
        data = array->values[index].is_null;
    }

    return data;
}
