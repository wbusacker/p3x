/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <math.h>
#include <stdio.h>

void json_export_value(struct Long_buffer* buffer, struct JSON_Value* value) {

    switch (value->datatype) {
        case JSON_STRING:
            json_export_string(buffer, &(value->string));
            break;
        case JSON_NUMBER: {
            uint8_t holding_buffer[JSON_NUMBER_EXPORT_DESTINATION_LENGTH];

            if (floor(value->number) == value->number) {
                snprintf(holding_buffer,
                         JSON_NUMBER_EXPORT_DESTINATION_LENGTH,
                         JSON_NUMBER_INTEGER_EXPORT_FORMAT,
                         (int64_t)value->number);
            } else {
                snprintf(holding_buffer,
                         JSON_NUMBER_EXPORT_DESTINATION_LENGTH,
                         JSON_NUMBER_FLOAT_EXPORT_FORMAT,
                         value->number);
            }
            long_buffer_append(buffer, holding_buffer, JSON_NUMBER_EXPORT_DESTINATION_LENGTH - 1);
        } break;
        case JSON_OBJECT:
            json_export_object(buffer, &(value->object));
            break;
        case JSON_ARRAY:
            json_export_array(buffer, &(value->array));
            break;
        case JSON_BOOL:
            long_buffer_append(buffer, value->boolean ? JSON_TRUE_STRING : JSON_FALSE_STRING, value->boolean ? 4 : 5);
            break;
        case JSON_NULL:
            long_buffer_append(buffer, JSON_NULL_STRING, 4);
            break;
        default:
            break;
    }
    return;
}
