/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_text_array.c
Purpose:  Imports a JSON array from text buffer
*/

#include <json_functions.h>
#include <stdio.h>
#include <stdlib.h>

struct JSON_Array* json_import_text_array(uint8_t* text, uint64_t length) {

    json_read_failure_index = NULL;

    struct JSON_Array* array = NULL;

    if (json_check_recmatch(text, length)) {
        array              = (struct JSON_Array*)malloc(sizeof(struct JSON_Array));
        uint8_t* text_scan = text;

        SKIP_WHITESPACE(text_scan);

        int32_t imported_bytes = json_import_array(text_scan, array);

        if (imported_bytes == JSON_BAD_IMPORT) {
            json_print_error_location(text);
            free(array);
            array = NULL;
        }
    } else {
        json_read_failure_index = 0;
    }

    return array;
}