/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>

void json_export_string(struct Long_buffer* buffer, struct JSON_String* string) {

    long_buffer_append(buffer, (uint8_t*)"\"", 1);

    for (uint64_t i = 0; i < string->string_length; i++) {

        bool special_handling = false;
        /* First, check to see if we need to escape this character code */
        for (uint8_t j = 0; j < NUM_JSON_ESCAPED_CHARACTERS; j++) {
            if (string->string[i] == JSON_ESCAPED_CODES[j]) {
                special_handling = true;
                long_buffer_append(buffer, (uint8_t*)"\\", 1);
                long_buffer_append(buffer, JSON_ESCAPED_CHARACTERS + j, 1);
            }
        }
        if (special_handling == false) {
            long_buffer_append(buffer, string->string + i, 1);
        }
    }

    long_buffer_append(buffer, (uint8_t*)"\"", 1);
}
