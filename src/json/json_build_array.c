/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_array.c
Purpose:  Imports a JSON array from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>

struct JSON_Array* json_build_array(void) {

    struct JSON_Array* new_array = (struct JSON_Array*)malloc(sizeof(struct JSON_Array));

    new_array->values     = NULL;
    new_array->num_values = 0;

    return new_array;
}