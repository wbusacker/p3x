/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: json_import_object.c
Purpose:  Imports a JSON object from text
*/

#include <json_functions.h>
#include <stdlib.h>
#include <string.h>
struct JSON_String* json_access_object_string(struct JSON_Object* object, char* key) {
    struct JSON_String* data  = NULL;
    uint32_t            index = json_get_object_keypair_index(object, key, JSON_STRING);

    if (index != JSON_INVALID_INDEX) {
        data = &(object->elements[index].value.string);
    }

    return data;
}

double json_access_object_number(struct JSON_Object* object, char* key) {
    double   data  = 0;
    uint32_t index = json_get_object_keypair_index(object, key, JSON_NUMBER);

    if (index != JSON_INVALID_INDEX) {
        data = object->elements[index].value.number;
    }

    return data;
}

struct JSON_Object* json_access_object_object(struct JSON_Object* object, char* key) {
    struct JSON_Object* data  = NULL;
    uint32_t            index = json_get_object_keypair_index(object, key, JSON_OBJECT);

    if (index != JSON_INVALID_INDEX) {
        data = &(object->elements[index].value.object);
    }

    return data;
}

struct JSON_Array* json_access_object_array(struct JSON_Object* object, char* key) {
    struct JSON_Array* data  = NULL;
    uint32_t           index = json_get_object_keypair_index(object, key, JSON_ARRAY);

    if (index != JSON_INVALID_INDEX) {
        data = &(object->elements[index].value.array);
    }

    return data;
}

bool json_access_object_boolean(struct JSON_Object* object, char* key) {
    bool     data  = NULL;
    uint32_t index = json_get_object_keypair_index(object, key, JSON_BOOL);

    if (index != JSON_INVALID_INDEX) {
        data = object->elements[index].value.boolean;
    }

    return data;
}

bool json_access_object_is_null(struct JSON_Object* object, char* key) {
    bool     data  = NULL;
    uint32_t index = json_get_object_keypair_index(object, key, JSON_NULL);

    if (index != JSON_INVALID_INDEX) {
        data = object->elements[index].value.is_null;
    }

    return data;
}
