/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: uptime_data.h
Purpose:  CSC data declaration
*/

#ifndef UPTIME_DATA_H
#define UPTIME_DATA_H
#include <stdint.h>
#include <uptime_const.h>
#include <uptime_types.h>

extern uint64_t uptime_time_at_start;

#endif
