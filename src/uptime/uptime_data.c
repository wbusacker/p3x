/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: uptime_data.h
Purpose:  CSC data declaration
*/

#include <uptime_data.h>

uint64_t uptime_time_at_start;