/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: uptime_functions.h
Purpose:  CSC local function declarations
*/

#ifndef UPTIME_FUNCTIONS_H
#define UPTIME_FUNCTIONS_H
#include <uptime.h>
#include <uptime_data.h>

uint64_t uptime_current_time_nanoseconds(void);

#endif
