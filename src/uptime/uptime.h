/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: uptime.h
Purpose:  External CSC Header
*/

#ifndef UPTIME_H
#define UPTIME_H
#include <stdint.h>
#include <uptime_const.h>
#include <uptime_types.h>

uint8_t init_uptime(void);
uint8_t teardown_uptime(void);

uint64_t uptime_nanoseconds(void);

#endif
