/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: uptime_const.h
Purpose:  CSC constants
*/

#ifndef UPTIME_CONST_H
#define UPTIME_CONST_H

#define NS_PER_US (1E3)
#define NS_PER_MS (1E6)
#define NS_PER_S  (1E9)

#define US_PER_MS (1E3)
#define US_PER_S  (1E6)

#define MS_PER_S (1E3)

#endif
