/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: uptime_nanoseconds.c
Purpose:  Get the current system time in nanoseconds
*/

#include <platform.h>
#include <uptime_functions.h>

#ifdef PLATFORM_IS_POSIX

    #include <time.h>

uint64_t uptime_nanoseconds(void) {

    struct timespec current_time;

    clock_gettime(CLOCK_MONOTONIC, &current_time);

    return (current_time.tv_sec * NS_PER_S) + current_time.tv_nsec;
}

#endif

#ifdef PLATFORM_IS_NT

    #include <windows.h>

uint64_t uptime_nanoseconds(void) {

    LARGE_INTEGER ticks;
    LARGE_INTEGER frequency;

    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&ticks);

    // printf("%ld\n", ticks.QuadPart);
    // printf("%ld\n", frequency.QuadPart);

    return (ticks.QuadPart * NS_PER_S) / frequency.QuadPart;
}
#endif