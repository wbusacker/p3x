/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: rate_monotonic.c
Purpose:  CSC data and initialization definitions
*/

#include <rate_monotonic_functions.h>

void rate_monotonic_init(struct Rate_monotonic_t* rm, uint64_t period_ns, uint64_t first_unlock_time_ns) {

    rm->period_ns      = period_ns;
    rm->missed_periods = 0;
    rm->total_periods  = 0;
    rm->next_unlock_ns = 0;

    if (first_unlock_time_ns == RATE_MONOTONIC_IMMEDIATE_UNLOCK) {
        rm->state = RATE_MONOTONIC_START_IMMEDIATE;
    } else {
        rm->state          = RATE_MONOTONIC_START_DELAYED;
        rm->next_unlock_ns = first_unlock_time_ns;
    }
}
