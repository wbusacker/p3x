/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: rate_monotonic_functions.h
Purpose:  CSC local function declarations
*/

#ifndef RATE_MONOTONIC_FUNCTIONS_H
#define RATE_MONOTONIC_FUNCTIONS_H
#include <rate_monotonic.h>
#include <rate_monotonic_data.h>

#endif
