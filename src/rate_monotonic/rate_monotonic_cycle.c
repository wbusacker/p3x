/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: rate_monotonic.c
Purpose:  CSC data and initialization definitions
*/

#include <platform.h>
#include <rate_monotonic_functions.h>
#include <uptime.h>

#if defined(PLATFORM_IS_POSIX)
    #include <unistd.h>
#elif defined(PLATFORM_IS_NT)
    #include <windows.h>
#endif

enum Rate_monotonic_state_enm rate_monotonic_cycle(struct Rate_monotonic_t* rm) {

    /* If we've never cycled before, figure out how to calculate the next unlock */
    if (rm->state == RATE_MONOTONIC_START_DELAYED) {
        rm->state = RATE_MONOTONIC_ENABLED;
    } else if (rm->state == RATE_MONOTONIC_START_IMMEDIATE) {
        rm->state = RATE_MONOTONIC_ENABLED;

        /* Just set the next unlock time to right now. When we evaluate the sleep
            time, we'll be negative and guarantee trigger a missed period so
            subtract one off
        */
        rm->next_unlock_ns = uptime_nanoseconds();
        rm->missed_periods = -1;
    }

    int64_t time_remaining = rm->next_unlock_ns - uptime_nanoseconds();

    /* Did we blow past the period? */
    if (time_remaining < 0) {
        rm->missed_periods++;
    } else {
        /* If we're still in range for the period, do we have to sleep? */
        if (time_remaining > RATE_MONOTONIC_SLEEP_THRESHOLD_NS) {

#if defined(PLATFORM_IS_POSIX)
            usleep(time_remaining / NS_PER_US);
#elif defined(PLATFORM_IS_NT)
            Sleep(time_remaining / NS_PER_MS);
#endif
        }
    }

    rm->total_periods++;
    rm->next_unlock_ns += rm->period_ns;
}
