/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: rate_monotonic_types.h
Purpose:  CSC data types
*/

#ifndef RATE_MONOTONIC_TYPES_H
#define RATE_MONOTONIC_TYPES_H
#include <rate_monotonic_const.h>
#include <stdint.h>

struct Rate_monotonic_t {
    uint64_t                      period_ns;
    uint64_t                      next_unlock_ns;
    uint64_t                      missed_periods;
    uint64_t                      total_periods;
    enum Rate_monotonic_state_enm state;
};

#endif
