/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: rate_monotonic.h
Purpose:  External CSC Header
*/

#ifndef RATE_MONOTONIC_H
#define RATE_MONOTONIC_H
#include <rate_monotonic_const.h>
#include <rate_monotonic_types.h>
#include <stdint.h>

uint8_t init_rate_monotonic(void);
uint8_t teardown_rate_monotonic(void);

void rate_monotonic_init(struct Rate_monotonic_t* rm, uint64_t period_ns, uint64_t first_unlock_time_ns);
void rate_monotonic_suspend(struct Rate_monotonic_t* rm);
void rate_monotonic_set_period(struct Rate_monotonic_t* rm, uint64_t period_ns);

enum Rate_monotonic_state_enm rate_monotonic_cycle(struct Rate_monotonic_t* rm);

#endif
