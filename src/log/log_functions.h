/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_functions.h
Purpose:  CSC local function declarations
*/

#ifndef LOG_FUNCTIONS_H
#define LOG_FUNCTIONS_H
#include <log.h>
#include <log_data.h>

uint32_t log_find_csc_index(const char* csc_pattern);

#endif
