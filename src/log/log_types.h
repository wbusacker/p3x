/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_types.h
Purpose:  CSC data types
*/

#ifndef LOG_TYPES_H
#define LOG_TYPES_H
#include <log_const.h>
#include <stdbool.h>

/*
    Levels of severity for the requested message.
    Rank in order such that when a log level is set,
    any level equal or lower value will be put to
    the log, all others will be dropped
*/

enum LOG_LEVELS {
    LOG_LEVEL_NEVER = 0,
    LOG_LEVEL_ALWAYS,
    LOG_LEVEL_CRITICAL,
    LOG_LEVEL_ERROR,
    LOG_LEVEL_WARNING,
    LOG_LEVEL_INFO,
    LOG_LEVEL_DEBUG,
    LOG_LEVEL_NUM_LEVELS
};

struct Log_level_csc {
    enum LOG_LEVELS level;
    bool            active;
    char            pattern[LOG_MAXIMUM_PATTERN_LEN];
};

#endif
