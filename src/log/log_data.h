/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_data.h
Purpose:  CSC data declaration
*/

#ifndef LOG_DATA_H
#define LOG_DATA_H
#include <log_const.h>
#include <log_types.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

extern FILE*    log_file_handle;
extern uint64_t log_time_start_ns;

extern char log_level_strings[LOG_LEVEL_NUM_LEVELS][LOG_LEVEL_STRING_LEN];

extern enum LOG_LEVELS      log_system_level;
extern struct Log_level_csc log_csc_levels[LOG_MAXIMUM_PATTERNS];

#endif
