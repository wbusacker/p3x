/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log.c
Purpose:  CSC data and initialization definitions
*/

#include <directory.h>
#include <log.h>
#include <log_functions.h>
#include <string.h>
#include <uptime.h>

uint8_t init_log(char* name) {

    static bool already_init = false;

    if (already_init == false) {
        already_init = true;
        init_uptime();

        for (uint32_t i = 0; i < LOG_MAXIMUM_PATTERNS; i++) {
            log_csc_levels[i].active = false;
            log_csc_levels[i].level  = LOG_LEVEL_NEVER;
            memset(log_csc_levels[i].pattern, 0, LOG_MAXIMUM_PATTERN_LEN);
        }

        log_system_level = LOG_LEVEL_INFO;

        log_time_start_ns = uptime_nanoseconds();

        char log_file_name[MAX_LEN_FILESYSTEM_NAME];
        memset(log_file_name, 0, MAX_LEN_FILESYSTEM_NAME);
        sprintf(log_file_name, "%s.log", name);

        log_file_handle = fopen(log_file_name, "w");

        LOG_INFO("Starting log\n");
    }

    return 0;
}

uint8_t teardown_log(void) {
    fclose(log_file_handle);
    return 0;
}
