/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log.h
Purpose:  External CSC Header
*/

#ifndef LOG_H
#define LOG_H
#include <log_const.h>
#include <log_types.h>
#include <stdint.h>

uint8_t init_log(char* name);
uint8_t teardown_log(void);

/* DEPRICATED: Use other Log functions */
#define LOG(...) log_printf(LOG_LEVEL_ALWAYS, __FILE__, __LINE__, __VA_ARGS__)

/* Send a message to program log always */
#define LOG_ALWAYS(...) log_printf(LOG_LEVEL_ALWAYS, __FILE__, __LINE__, __VA_ARGS__)

/* Send a critical message to program log. Use for fatal errors */
#define LOG_CRITICAL(...) log_printf(LOG_LEVEL_CRITICAL, __FILE__, __LINE__, __VA_ARGS__)

/* Send a general error message to program log. Use for recoverable errors */
#define LOG_ERROR(...) log_printf(LOG_LEVEL_ERROR, __FILE__, __LINE__, __VA_ARGS__)

/* Send a warning message to program log. */
#define LOG_WARNING(...) log_printf(LOG_LEVEL_WARNING, __FILE__, __LINE__, __VA_ARGS__)

/* Send a general message to program log. */
#define LOG_INFO(...) log_printf(LOG_LEVEL_INFO, __FILE__, __LINE__, __VA_ARGS__)

/* Good old PRINTF Debugging */
#define LOG_DEBUG(...) log_printf(LOG_LEVEL_DEBUG, __FILE__, __LINE__, __VA_ARGS__)

void log_printf(enum LOG_LEVELS level, const char* file_name, int line_number, const char* format, ...);

void log_set_global_level(enum LOG_LEVELS level);
void log_set_csc_level(enum LOG_LEVELS level, char* csc_pattern);

#endif
