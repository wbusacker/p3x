/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_const.h
Purpose:  CSC constants
*/

#ifndef LOG_CONST_H
#define LOG_CONST_H

#include <platform.h>

#define LOG_LEVEL_STRING_LEN (5)

#if defined(PLATFORM_IS_POSIX)
    #define LOG_STANDARD_HEADER_FORMAT "%15ld | %s | %50s:%-4d | "
#elif defined(PLATFORM_IS_NT)
    #define LOG_STANDARD_HEADER_FORMAT "%15lld | %s | %50s:%-4d | "
#endif

#define LOG_MAXIMUM_PATTERN_LEN (64)
#define LOG_MAXIMUM_PATTERNS    (64)

#define LOG_INVALID_PATTERN_ID LOG_MAXIMUM_PATTERNS

#endif
