/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_template.c
Purpose:  Example Function File
*/

#include <log_functions.h>
#include <string.h>

void log_set_csc_level(enum LOG_LEVELS level, char* csc_pattern) {

    uint32_t csc_index = log_find_csc_index(csc_pattern);

    /* Check if we already know about this CSC, if not, add an entry for it */
    if (csc_index == LOG_INVALID_PATTERN_ID) {
        for (csc_index = 0; csc_index < LOG_MAXIMUM_PATTERNS; csc_index++) {
            if (log_csc_levels[csc_index].active == false) {
                log_csc_levels[csc_index].active = true;
                strncpy(log_csc_levels[csc_index].pattern, csc_pattern, LOG_MAXIMUM_PATTERN_LEN - 1);
                /* Always ensure a null terminate */
                log_csc_levels[csc_index].pattern[LOG_MAXIMUM_PATTERN_LEN - 1] = 0;
            }
        }
    }

    /* If we still have an invalid pattern ID it means we ran out of slots, so just skip this */
    if (csc_index != LOG_INVALID_PATTERN_ID) {
        log_csc_levels[csc_index].level = level;
    }
}