/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_template.c
Purpose:  Example Function File
*/

#include <log_functions.h>

void log_set_global_level(enum LOG_LEVELS level) {
    log_system_level = level;
}