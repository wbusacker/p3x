/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_data.c
Purpose:  CSC data definition
*/

#include <log_data.h>
#include <stdlib.h>

FILE*    log_file_handle   = NULL;
uint64_t log_time_start_ns = 0;

char log_level_strings[LOG_LEVEL_NUM_LEVELS][LOG_LEVEL_STRING_LEN] =
  {"NEV ", "ALWY", "CRIT", "ERR ", "WARN", "INFO", "DBG "};

enum LOG_LEVELS      log_system_level;
struct Log_level_csc log_csc_levels[LOG_MAXIMUM_PATTERNS];