/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_template.c
Purpose:  Example Function File
*/

#include <log_functions.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <uptime.h>

void log_printf(enum LOG_LEVELS level, const char* file_name, int line_number, const char* format, ...) {

    /* Figure out if we should surpress this or not */
    if (level <= log_system_level) {

        bool csc_allowed = true;

        /* The system will at least let this through, check for the CSC from the file name */
        uint32_t csc_index = log_find_csc_index(file_name);
        if (csc_index != LOG_INVALID_PATTERN_ID) {
            if (log_csc_levels[csc_index].active && (level > log_csc_levels[csc_index].level)) {
                csc_allowed = false;
            }
        }

        if (csc_allowed) {

            va_list arg_list;
            va_start(arg_list, format);

            fprintf(log_file_handle,
                    LOG_STANDARD_HEADER_FORMAT,
                    uptime_nanoseconds() - log_time_start_ns,
                    log_level_strings[level],
                    file_name,
                    line_number);

            vfprintf(log_file_handle, format, arg_list);

            va_end(arg_list);

            fflush(log_file_handle);
        }
    }
}