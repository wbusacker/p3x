/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  p3x
Filename: log_template.c
Purpose:  Example Function File
*/

#include <log_functions.h>
#include <string.h>

uint32_t log_find_csc_index(const char* csc_pattern) {

    uint32_t index = LOG_INVALID_PATTERN_ID;

    for (uint32_t i = 0; i < LOG_MAXIMUM_PATTERNS; i++) {
        if (log_csc_levels[i].active) {
            if (strstr(csc_pattern, log_csc_levels[i].pattern) != NULL) {
                index = i;
                break;
            }
        }
    }

    return index;
}