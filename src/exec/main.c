/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  P3X
Filename: main.c
Purpose:  Program entry point
*/

int main(int argc, char** argv) {

    /* Given that this a library codebase, main is only here as a link check */

    return 0;
}
