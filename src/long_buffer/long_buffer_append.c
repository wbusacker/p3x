/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_template.c
Purpose:  Example Function File
*/

#include <long_buffer_functions.h>

void long_buffer_append(struct Long_buffer* buffer, const uint8_t* data, uint64_t data_len) {

    struct Long_buffer_fragment* fragment = buffer->fragment_list_tail;

    uint64_t data_copied = 0;
    while (data_copied < data_len) {

        for (; (fragment->data_len < LONG_BUFFER_FRAGMENT_DATA_LEN) && (data_copied < data_len);
             fragment->data_len++, data_copied++) {
            fragment->data[fragment->data_len] = data[data_copied];
            buffer->total_data_len++;
        }

        if (data_copied < data_len) {
            fragment->next_fragment    = long_buffer_build_fragment();
            buffer->fragment_list_tail = fragment->next_fragment;
            fragment                   = fragment->next_fragment;
        }
    }
}