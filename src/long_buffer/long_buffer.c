/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer.c
Purpose:  CSC data and initialization definitions
*/

#include <long_buffer.h>
#include <long_buffer_functions.h>

uint8_t init_long_buffer(void) {
    return 0;
}

uint8_t teardown_long_buffer(void) {
    return 0;
}
