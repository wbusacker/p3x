/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_types.h
Purpose:  CSC data types
*/

#ifndef LONG_BUFFER_TYPES_H
#define LONG_BUFFER_TYPES_H
#include <long_buffer_const.h>
#include <stdint.h>

struct Long_buffer_fragment {
    uint8_t                      data[LONG_BUFFER_FRAGMENT_DATA_LEN];
    uint16_t                     data_len;
    struct Long_buffer_fragment* next_fragment;
};

struct Long_buffer {
    struct Long_buffer_fragment* fragment_list_head;
    struct Long_buffer_fragment* fragment_list_tail;
    uint64_t                     total_data_len;
};

#endif
