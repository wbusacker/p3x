/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_template.c
Purpose:  Example Function File
*/

#include <long_buffer_functions.h>
#include <stdlib.h>
#include <string.h>

struct Long_buffer_fragment* long_buffer_build_fragment(void) {

    struct Long_buffer_fragment* fragment = (struct Long_buffer_fragment*)malloc(sizeof(struct Long_buffer_fragment));

    memset(fragment->data, 0, LONG_BUFFER_FRAGMENT_DATA_LEN);
    fragment->data_len      = 0;
    fragment->next_fragment = NULL;

    return fragment;
}