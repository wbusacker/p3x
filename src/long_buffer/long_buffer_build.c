/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_template.c
Purpose:  Example Function File
*/

#include <long_buffer_functions.h>
#include <stdlib.h>

struct Long_buffer* long_buffer_build(void) {

    struct Long_buffer* buffer = (struct Long_buffer*)malloc(sizeof(struct Long_buffer));
    buffer->fragment_list_head = long_buffer_build_fragment();
    buffer->fragment_list_tail = buffer->fragment_list_head;

    buffer->total_data_len = 0;

    return buffer;
}