/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_template.c
Purpose:  Example Function File
*/

#include <long_buffer_functions.h>
#include <stdlib.h>

uint64_t long_buffer_copy_out(struct Long_buffer* buffer, uint8_t* data, uint64_t max_len) {

    struct Long_buffer_fragment* fragment = buffer->fragment_list_head;

    uint64_t data_copied = 0;
    while ((data_copied < max_len) && (fragment != NULL)) {

        for (uint64_t i = 0; ((i < fragment->data_len) && (data_copied <= max_len)); data_copied++, i++) {
            data[data_copied] = fragment->data[i];
        }

        fragment = fragment->next_fragment;
    }

    return data_copied;
}