/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_const.h
Purpose:  CSC constants
*/

#ifndef LONG_BUFFER_CONST_H
#define LONG_BUFFER_CONST_H

#define LONG_BUFFER_FRAGMENT_DATA_LEN (1024)

#endif
