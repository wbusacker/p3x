/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_functions.h
Purpose:  CSC local function declarations
*/

#ifndef LONG_BUFFER_FUNCTIONS_H
#define LONG_BUFFER_FUNCTIONS_H
#include <long_buffer.h>
#include <long_buffer_data.h>

struct Long_buffer_fragment* long_buffer_build_fragment(void);

#endif
