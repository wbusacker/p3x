/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_data.h
Purpose:  CSC data declaration
*/

#ifndef LONG_BUFFER_DATA_H
#define LONG_BUFFER_DATA_H
#include <long_buffer_const.h>
#include <long_buffer_types.h>

#endif
