/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer_template.c
Purpose:  Example Function File
*/

#include <long_buffer_functions.h>
#include <stdlib.h>

void long_buffer_delete(struct Long_buffer* buffer) {

    struct Long_buffer_fragment* fragment = buffer->fragment_list_head;
    while (fragment != NULL) {
        struct Long_buffer_fragment* next = fragment->next_fragment;
        free(fragment);
        fragment = next;
    }

    free(buffer);
}