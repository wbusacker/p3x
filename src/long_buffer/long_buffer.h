/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  p3x
Filename: long_buffer.h
Purpose:  External CSC Header
*/

#ifndef LONG_BUFFER_H
#define LONG_BUFFER_H
#include <long_buffer_const.h>
#include <long_buffer_types.h>
#include <stdint.h>

uint8_t init_long_buffer(void);
uint8_t teardown_long_buffer(void);

struct Long_buffer* long_buffer_build(void);
void                long_buffer_delete(struct Long_buffer* buffer);
void                long_buffer_append(struct Long_buffer* buffer, const uint8_t* data, uint64_t data_len);
uint64_t            long_buffer_copy_out(struct Long_buffer* buffer, uint8_t* data, uint64_t max_len);

#endif
