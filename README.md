# Practical Portable Programming Extensions (P3X)

This is a starting point for GCC based C programs with a suite of build options

## Expected Code Structure

The build system assumes a semi-flat source code organization. Code is expected to be organized into logical units following DOD-STD-2167A sections 4.2.5 and 4.2.9, namely utilizing the concept of a Computer Software Component (CSC) as a basis of encapsulation and categorization of logic. The source and test directories are expected to be filled with CSC directories, the contents of which are a flat source file layout with both header files and source files intermixed. 

## Header Inclusion

Due to the expected code organization, all CSC folders are added into the compilation command allowing for the `#include <>` format to be used for including header files

# CSC Organization

## Externally Visible Files
### csc_const.h

Any non-mutable symbols for the CSC.

Includes language features like

- Preprocessor `#defines`
- Enumerations

### csc_types.h

Data structures/type overrides for the CSC

Includes language features 

- Structs
- Unions
- `typedef`

### csc.h

File that is included to be used by any external CSC

Includes language features:

- Function prototypes

## Intenally Visibile Files

### csc_functions.h

File that is included for all internal csc function files

Includes language features:

- Function prototypes

### csc_data.[c|h]

Definition/Declaration space for global CSC data

### csc_cycle.c

Function definition for the main cycle operation, enough code to compile and link against but does nothing of interest.

### csc.c

Function definitions for csc_init and csc_teardown, enough code to compile and link against but does nothing of interest.

### csc_template.c

Blank file that contains all boilerplate for each individual function

## Test Files

### t_csc_example.c

Blank file that contains all boilerplate to create test cases