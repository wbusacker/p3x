#!/usr/bin/python3

from ast import Mod
import sys
import os
import datetime

COPYRIGHT_USER = "Will Busacker"

GLOBAL_HEADER='''/*
Copyright (c) {year} {author}
See project license for more details

Project:  {project}
Filename: {file_name}
Purpose:  {purpose}
*/

'''

class Module_file:

    TEST = 0
    MAIN = 1

    def __init__(self, project_file_type, file_postfix, description, file_template):

        mnl = sys.argv[1].lower()
        mnu = mnl.upper()

        code_file_dir = "./src/{0}/".format(mnl)
        full_file_name = code_file_dir + mnl + file_postfix

        if(project_file_type == Module_file.TEST):
            code_file_dir ="./test/{0}/".format(mnl)
            full_file_name = code_file_dir + "t_" + mnl + file_postfix

        # If the directory doesn't exist, make it
        if os.path.isdir(code_file_dir) == False:
            os.mkdir(code_file_dir)

        with open(full_file_name, "w") as fh:
            # Write the global copyright header
            fh.write(GLOBAL_HEADER.format(year    = datetime.date.today().year,
                                          author  = COPYRIGHT_USER,
                                          project = os.getcwd().split("/")[-1],
                                          file_name = (mnl + file_postfix),
                                          purpose = description))
            fh.write(file_template.format(module_name_u = mnu, module_name_l = mnl))


if(len(sys.argv) != 2):
    print("Usage: {0} module".format(sys.argv[0]))
    exit(-1)

Module_file(Module_file.MAIN, "_data.h", "CSC data declaration",
"""#ifndef {module_name_u}_DATA_H
#define {module_name_u}_DATA_H
#include <{module_name_l}_types.h>
#include <{module_name_l}_const.h>



#endif
"""
)

Module_file(Module_file.MAIN, "_const.h", "CSC constants",
"""#ifndef {module_name_u}_CONST_H
#define {module_name_u}_CONST_H



#endif
"""
)

Module_file(Module_file.MAIN, "_types.h", "CSC data types",
"""#ifndef {module_name_u}_TYPES_H
#define {module_name_u}_TYPES_H
#include <{module_name_l}_const.h>



#endif
"""
)

Module_file(Module_file.MAIN, "_functions.h", "CSC local function declarations",
"""#ifndef {module_name_u}_FUNCTIONS_H
#define {module_name_u}_FUNCTIONS_H
#include <{module_name_l}.h>
#include <{module_name_l}_data.h>



#endif
"""
)

Module_file(Module_file.MAIN, ".h", "External CSC Header",
"""#ifndef {module_name_u}_H
#define {module_name_u}_H
#include <{module_name_l}_types.h>
#include <{module_name_l}_const.h>

#include <stdint.h>

uint8_t init_{module_name_l}(void);
uint8_t teardown_{module_name_l}(void);

#endif
"""
)

Module_file(Module_file.MAIN, ".c", "CSC data and initialization definitions",
"""#include <{module_name_l}.h>
#include <{module_name_l}_data.h>

uint8_t init_{module_name_l}(void){{

}}

uint8_t teardown_{module_name_l}(void){{

}}
"""
)


Module_file(Module_file.TEST, "_example.cpp", "CSC gtest module",
"""#include <gtest/gtest.h>
#include <gmock/gmock.h>

extern "C" {{
#include<{module_name_l}_functions.h>
}}

"""
)
